// halaman cetak drd : print

// tagihan bulan ini
var bulan	= parseInt(dataFeed.filter[0].value);
var tahun	= dataFeed.filter[1].value;
inHTML		= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-tagihan').html(inHTML);
// rekening bulan ini
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-1').html(inHTML);
// rekening bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
if(bulan>2){
	inHTML	= namaBulan(bulan);
}
else{
	inHTML	= namaBulan(bulan) + ' ' + tahun;
}
jQuery('.bulan-rekening-2').html(inHTML);
// rekening 2 bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-3').html(inHTML);

// ambil data referensi
targetUrl	= "/api/perpamsi/referensi/view_reff.php";
dataFeed.filter.push({name: 'tt', value: 'BA' + dataFeed.filter[2].value});
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		jQuery('span.' + value.tc).html(value.tdesc);
		jQuery('input.' + value.tc + '[name=tdesc]').val(value.tdesc);
	});
}, 'json');

// ambil data rekening
targetUrl				= "/api/perpamsi/rekening/kemhan/bulan/view_rekap.php";
var klarifikasiVolume	= 0;
var klarifikasiRupiah	= 0;
var diajukanVolume00	= 0;
var diajukanRupiah00	= 0;
var diajukanVolume01	= 0;
var diajukanRupiah01	= 0;
var diajukanVolume02	= 0;
var diajukanRupiah02	= 0;
var diajukanVolume04	= 0;
var diajukanRupiah04	= 0;
var volume				= 0;
var rupiah				= 0;
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		klarifikasiVolume	= klarifikasiVolume + parseInt(value.klarifikasi_st_pakai);
		klarifikasiRupiah	= klarifikasiRupiah + parseInt(value.klarifikasi_total);
		diajukanVolume00	= diajukanVolume00 + parseInt(value.rek_st_pakai00);
		diajukanRupiah00	= diajukanRupiah00 + parseInt(value.rek_total00);
		diajukanVolume01	= diajukanVolume01 + parseInt(value.rek_st_pakai01);
		diajukanRupiah01	= diajukanRupiah01 + parseInt(value.rek_total01);
		diajukanVolume02	= diajukanVolume02 + parseInt(value.rek_st_pakai02);
		diajukanRupiah02	= diajukanRupiah02 + parseInt(value.rek_total02);
		diajukanVolume04	= diajukanVolume04 + parseInt(value.rek_st_pakai04);
		diajukanRupiah04	= diajukanRupiah04 + parseInt(value.rek_total04);
		volume				= volume + parseInt(value.rek_st_pakai);
		rupiah				= rupiah + parseInt(value.rek_total);
		switch(value.gp_kode){
			// 04 : tagihan sampai dengan 2 bulan lalu
			// 01 : tagihan bulan lalu
			// 00 : tagihan bulan ini
			// data kemhan
			case '010101':
				// tagihan hasil klarifikasi
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-klarifikasi-kemhan').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-klarifikasi-kemhan').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}

				// tagihan seluruhnya
				if(value.rek_st_pakai>0){
					jQuery('.volume-kemhan').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-kemhan').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}

				// 04: tagihan susulan
				if(value.rek_st_pakai04>0){
					jQuery('.volume-04-kemhan').html(jQuery.formatNumber(value.rek_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-04-kemhan').html(jQuery.formatNumber(value.rek_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tunda-04-kemhan').html(jQuery.formatNumber(value.tunda_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tunda-04-kemhan').html(jQuery.formatNumber(value.tunda_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tolak-04-kemhan').html(jQuery.formatNumber(value.tolak_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tolak-04-kemhan').html(jQuery.formatNumber(value.tolak_total04, {format:'#,###', locale:'us'}));
				}

				// 02 : tagihan 2 bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai02>0){
					jQuery('.volume-02-kemhan').html(jQuery.formatNumber(value.rek_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-02-kemhan').html(jQuery.formatNumber(value.rek_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tunda-02-kemhan').html(jQuery.formatNumber(value.tunda_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tunda-02-kemhan').html(jQuery.formatNumber(value.tunda_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tolak-02-kemhan').html(jQuery.formatNumber(value.tolak_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tolak-02-kemhan').html(jQuery.formatNumber(value.tolak_total02, {format:'#,###', locale:'us'}));
				}

				// 01 : tagihan bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai01>0){
					jQuery('.volume-01-kemhan').html(jQuery.formatNumber(value.rek_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-01-kemhan').html(jQuery.formatNumber(value.rek_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tunda-01-kemhan').html(jQuery.formatNumber(value.tunda_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tunda-01-kemhan').html(jQuery.formatNumber(value.tunda_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tolak-01-kemhan').html(jQuery.formatNumber(value.tolak_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tolak-01-kemhan').html(jQuery.formatNumber(value.tolak_total01, {format:'#,###', locale:'us'}));
				}

				// 00 : tagihan bulan ini
				// tagihan diajukan
				if(value.rek_st_pakai00>0){
					jQuery('.volume-00-kemhan').html(jQuery.formatNumber(value.rek_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-00-kemhan').html(jQuery.formatNumber(value.rek_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tunda-00-kemhan').html(jQuery.formatNumber(value.tunda_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tunda-00-kemhan').html(jQuery.formatNumber(value.tunda_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tolak-00-kemhan').html(jQuery.formatNumber(value.tolak_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tolak-00-kemhan').html(jQuery.formatNumber(value.tolak_total00, {format:'#,###', locale:'us'}));
				}
				break;
			// data mabes
			case '010201':
				// tagihan hasil klarifikasi
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-klarifikasi-mabes').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-klarifikasi-mabes').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}

				// tagihan seluruhnya
				if(value.rek_st_pakai>0){
					jQuery('.volume-mabes').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-mabes').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}

				// 04: tagihan susulan
				if(value.rek_st_pakai04>0){
					jQuery('.volume-04-mabes').html(jQuery.formatNumber(value.rek_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-04-mabes').html(jQuery.formatNumber(value.rek_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tunda-04-mabes').html(jQuery.formatNumber(value.tunda_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tunda-04-mabes').html(jQuery.formatNumber(value.tunda_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tolak-04-mabes').html(jQuery.formatNumber(value.tolak_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tolak-04-mabes').html(jQuery.formatNumber(value.tolak_total04, {format:'#,###', locale:'us'}));
				}

				// 02 : tagihan 2 bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai02>0){
					jQuery('.volume-02-mabes').html(jQuery.formatNumber(value.rek_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-02-mabes').html(jQuery.formatNumber(value.rek_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tunda-02-mabes').html(jQuery.formatNumber(value.tunda_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tunda-02-mabes').html(jQuery.formatNumber(value.tunda_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tolak-02-mabes').html(jQuery.formatNumber(value.tolak_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tolak-02-mabes').html(jQuery.formatNumber(value.tolak_total02, {format:'#,###', locale:'us'}));
				}

				// 01 : tagihan bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai01>0){
					jQuery('.volume-01-mabes').html(jQuery.formatNumber(value.rek_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-01-mabes').html(jQuery.formatNumber(value.rek_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tunda-01-mabes').html(jQuery.formatNumber(value.tunda_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tunda-01-mabes').html(jQuery.formatNumber(value.tunda_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tolak-01-mabes').html(jQuery.formatNumber(value.tolak_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tolak-01-mabes').html(jQuery.formatNumber(value.tolak_total01, {format:'#,###', locale:'us'}));
				}

				// 00 : tagihan bulan ini
				// tagihan diajukan
				if(value.rek_st_pakai00>0){
					jQuery('.volume-00-mabes').html(jQuery.formatNumber(value.rek_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-00-mabes').html(jQuery.formatNumber(value.rek_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tunda-00-mabes').html(jQuery.formatNumber(value.tunda_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tunda-00-mabes').html(jQuery.formatNumber(value.tunda_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tolak-00-mabes').html(jQuery.formatNumber(value.tolak_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tolak-00-mabes').html(jQuery.formatNumber(value.tolak_total00, {format:'#,###', locale:'us'}));
				}
				break;
			// data tna ad
			case '010300':
				// tagihan hasil klarifikasi
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-klarifikasi-ad').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-klarifikasi-ad').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}

				// tagihan seluruhnya
				if(value.rek_st_pakai>0){
					jQuery('.volume-ad').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-ad').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}

				// 04: tagihan susulan
				if(value.rek_st_pakai04>0){
					jQuery('.volume-04-ad').html(jQuery.formatNumber(value.rek_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-04-ad').html(jQuery.formatNumber(value.rek_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tunda-04-ad').html(jQuery.formatNumber(value.tunda_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tunda-04-ad').html(jQuery.formatNumber(value.tunda_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tolak-04-ad').html(jQuery.formatNumber(value.tolak_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tolak-04-ad').html(jQuery.formatNumber(value.tolak_total04, {format:'#,###', locale:'us'}));
				}

				// 02 : tagihan 2 bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai02>0){
					jQuery('.volume-02-ad').html(jQuery.formatNumber(value.rek_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-02-ad').html(jQuery.formatNumber(value.rek_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tunda-02-ad').html(jQuery.formatNumber(value.tunda_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tunda-02-ad').html(jQuery.formatNumber(value.tunda_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tolak-02-ad').html(jQuery.formatNumber(value.tolak_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tolak-02-ad').html(jQuery.formatNumber(value.tolak_total02, {format:'#,###', locale:'us'}));
				}

				// 01 : tagihan bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai01>0){
					jQuery('.volume-01-ad').html(jQuery.formatNumber(value.rek_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-01-ad').html(jQuery.formatNumber(value.rek_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tunda-01-ad').html(jQuery.formatNumber(value.tunda_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tunda-01-ad').html(jQuery.formatNumber(value.tunda_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tolak-01-ad').html(jQuery.formatNumber(value.tolak_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tolak-01-ad').html(jQuery.formatNumber(value.tolak_total01, {format:'#,###', locale:'us'}));
				}

				// 00 : tagihan bulan ini
				// tagihan diajukan
				if(value.rek_st_pakai00>0){
					jQuery('.volume-00-ad').html(jQuery.formatNumber(value.rek_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-00-ad').html(jQuery.formatNumber(value.rek_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tunda-00-ad').html(jQuery.formatNumber(value.tunda_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tunda-00-ad').html(jQuery.formatNumber(value.tunda_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tolak-00-ad').html(jQuery.formatNumber(value.tolak_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tolak-00-ad').html(jQuery.formatNumber(value.tolak_total00, {format:'#,###', locale:'us'}));
				}
				break;
			// data tna al
			case '010400':
				// tagihan hasil klarifikasi
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-klarifikasi-al').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-klarifikasi-al').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}

				// tagihan seluruhnya
				if(value.rek_st_pakai>0){
					jQuery('.volume-al').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-al').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}

				// 04: tagihan susulan
				if(value.rek_st_pakai04>0){
					jQuery('.volume-04-al').html(jQuery.formatNumber(value.rek_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-04-al').html(jQuery.formatNumber(value.rek_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tunda-04-al').html(jQuery.formatNumber(value.tunda_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tunda-04-al').html(jQuery.formatNumber(value.tunda_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tolak-04-al').html(jQuery.formatNumber(value.tolak_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tolak-04-al').html(jQuery.formatNumber(value.tolak_total04, {format:'#,###', locale:'us'}));
				}

				// 02 : tagihan 2 bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai02>0){
					jQuery('.volume-02-al').html(jQuery.formatNumber(value.rek_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-02-al').html(jQuery.formatNumber(value.rek_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tunda-02-al').html(jQuery.formatNumber(value.tunda_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tunda-02-al').html(jQuery.formatNumber(value.tunda_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tolak-02-al').html(jQuery.formatNumber(value.tolak_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tolak-02-al').html(jQuery.formatNumber(value.tolak_total02, {format:'#,###', locale:'us'}));
				}

				// 01 : tagihan bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai01>0){
					jQuery('.volume-01-al').html(jQuery.formatNumber(value.rek_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-01-al').html(jQuery.formatNumber(value.rek_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tunda-01-al').html(jQuery.formatNumber(value.tunda_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tunda-01-al').html(jQuery.formatNumber(value.tunda_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tolak-01-al').html(jQuery.formatNumber(value.tolak_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tolak-01-al').html(jQuery.formatNumber(value.tolak_total01, {format:'#,###', locale:'us'}));
				}

				// 00 : tagihan bulan ini
				// tagihan diajukan
				if(value.rek_st_pakai00>0){
					jQuery('.volume-00-al').html(jQuery.formatNumber(value.rek_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-00-al').html(jQuery.formatNumber(value.rek_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tunda-00-al').html(jQuery.formatNumber(value.tunda_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tunda-00-al').html(jQuery.formatNumber(value.tunda_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tolak-00-al').html(jQuery.formatNumber(value.tolak_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tolak-00-al').html(jQuery.formatNumber(value.tolak_total00, {format:'#,###', locale:'us'}));
				}
				break;
			// data tna au
			case '010500':
				// tagihan hasil klarifikasi
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-klarifikasi-au').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-klarifikasi-au').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}

				// tagihan seluruhnya
				if(value.rek_st_pakai>0){
					jQuery('.volume-au').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-au').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}

				// 04: tagihan susulan
				if(value.rek_st_pakai04>0){
					jQuery('.volume-04-au').html(jQuery.formatNumber(value.rek_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-04-au').html(jQuery.formatNumber(value.rek_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tunda-04-au').html(jQuery.formatNumber(value.tunda_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tunda-04-au').html(jQuery.formatNumber(value.tunda_total04, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai04>0){
					jQuery('.volume-tolak-04-au').html(jQuery.formatNumber(value.tolak_st_pakai04, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total04>0){
					jQuery('.rupiah-tolak-04-au').html(jQuery.formatNumber(value.tolak_total04, {format:'#,###', locale:'us'}));
				}

				// 02 : tagihan 2 bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai02>0){
					jQuery('.volume-02-au').html(jQuery.formatNumber(value.rek_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-02-au').html(jQuery.formatNumber(value.rek_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tunda-02-au').html(jQuery.formatNumber(value.tunda_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tunda-02-au').html(jQuery.formatNumber(value.tunda_total02, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai02>0){
					jQuery('.volume-tolak-02-au').html(jQuery.formatNumber(value.tolak_st_pakai02, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total02>0){
					jQuery('.rupiah-tolak-02-au').html(jQuery.formatNumber(value.tolak_total02, {format:'#,###', locale:'us'}));
				}

				// 01 : tagihan bulan lalu
				// tagihan diajukan
				if(value.rek_st_pakai01>0){
					jQuery('.volume-01-au').html(jQuery.formatNumber(value.rek_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-01-au').html(jQuery.formatNumber(value.rek_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tunda-01-au').html(jQuery.formatNumber(value.tunda_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tunda-01-au').html(jQuery.formatNumber(value.tunda_total01, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai01>0){
					jQuery('.volume-tolak-01-au').html(jQuery.formatNumber(value.tolak_st_pakai01, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total01>0){
					jQuery('.rupiah-tolak-01-au').html(jQuery.formatNumber(value.tolak_total01, {format:'#,###', locale:'us'}));
				}

				// 00 : tagihan bulan ini
				// tagihan diajukan
				if(value.rek_st_pakai00>0){
					jQuery('.volume-00-au').html(jQuery.formatNumber(value.rek_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-00-au').html(jQuery.formatNumber(value.rek_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditunda
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tunda-00-au').html(jQuery.formatNumber(value.tunda_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tunda-00-au').html(jQuery.formatNumber(value.tunda_total00, {format:'#,###', locale:'us'}));
				}
				// tagihan ditolak
				if(value.rek_st_pakai00>0){
					jQuery('.volume-tolak-00-au').html(jQuery.formatNumber(value.tolak_st_pakai00, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total00>0){
					jQuery('.rupiah-tolak-00-au').html(jQuery.formatNumber(value.tolak_total00, {format:'#,###', locale:'us'}));
				}
				break;
			default:
		}
	});

	// total klarifikasi
	if(klarifikasiVolume>0){
		jQuery('.volume-klarifikasi').html(jQuery.formatNumber(klarifikasiVolume, {format:'#,###', locale:'us'}));
	}
	if(klarifikasiRupiah>0){
		jQuery('.rupiah-klarifikasi').html(jQuery.formatNumber(klarifikasiRupiah, {format:'#,###', locale:'us'}));
	}
	// total diajukan dua bulan lalu
	if(diajukanVolume02>0){
		jQuery('.volume-02').html(jQuery.formatNumber(diajukanVolume02, {format:'#,###', locale:'us'}));
	}
	if(diajukanRupiah02>0){
		jQuery('.rupiah-02').html(jQuery.formatNumber(diajukanRupiah02, {format:'#,###', locale:'us'}));
	}
	// total diajukan bulan lalu
	if(diajukanVolume01>0){
		jQuery('.volume-01').html(jQuery.formatNumber(diajukanVolume01, {format:'#,###', locale:'us'}));
	}
	if(diajukanRupiah01>0){
		jQuery('.rupiah-01').html(jQuery.formatNumber(diajukanRupiah01, {format:'#,###', locale:'us'}));
	}
	// total diajukan ini
	if(diajukanVolume00>0){
		jQuery('.volume-00').html(jQuery.formatNumber(diajukanVolume00, {format:'#,###', locale:'us'}));
	}
	if(diajukanRupiah00>0){
		jQuery('.rupiah-00').html(jQuery.formatNumber(diajukanRupiah00, {format:'#,###', locale:'us'}));
	}
	// total diajukan bulan susulan
	if(diajukanVolume04>0){
		jQuery('.volume-04').html(jQuery.formatNumber(diajukanVolume04, {format:'#,###', locale:'us'}));
	}
	if(diajukanRupiah04>0){
		jQuery('.rupiah-04').html(jQuery.formatNumber(diajukanRupiah04, {format:'#,###', locale:'us'}));
	}
	// tagihan seluruhnya
	if(volume>0){
		jQuery('.volume').html(jQuery.formatNumber(volume, {format:'#,###', locale:'us'}));
	}
	if(rupiah>0){
		jQuery('.rupiah').html(jQuery.formatNumber(rupiah, {format:'#,###', locale:'us'}));
	}

}, 'json');
