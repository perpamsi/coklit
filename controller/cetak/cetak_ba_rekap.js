// tagihan bulan ini
var bulan	= parseInt(dataFeed.filter[0].value);
var tahun	= dataFeed.filter[1].value;
inHTML		= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-tagihan').html(inHTML);
// rekening bulan ini
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-1').html(inHTML);
// rekening bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
if(bulan>3){
	inHTML	= namaBulan(bulan);
}
else{
	inHTML	= namaBulan(bulan) + ' ' + tahun;
}
jQuery('.bulan-rekening-2').html(inHTML);
// rekening 2 bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-3').html(inHTML);

// ambil data referensi
targetUrl	= "/api/perpamsi/referensi/view_reff.php";
dataFeed.filter.push({name: 'tt', value: 'BA' + dataFeed.filter[2].value});
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		jQuery('span.' + value.tc).html(value.tdesc);
		jQuery('input.' + value.tc + '[name=tdesc]').val(value.tdesc);
	});
}, 'json');

targetUrl	= "/api/perpamsi/rekening/kemhan/view_rekap.php";
jQuery.post(targetUrl, {}, function(data){
	var totalVolDiklarifikasi	= 0;
	var totalRupDiklarifikasi	= 0;
	var totalVolDiajukan		= 0;
	var totalRupDiajukan		= 0;
	var totalVolDitunda			= 0;
	var totalRupDitunda			= 0;
	var totalVolDitolak			= 0;
	var totalRupDitolak			= 0;
	jQuery.each(data,function(i,value){
		switch(value.gp_kode){
			case '010101':
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-diklarifikasi-kemhan').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-diklarifikasi-kemhan').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}
				if(value.rek_st_pakai>0){
					jQuery('.volume-diajukan-kemhan').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-diajukan-kemhan').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_st_pakai>0){
					jQuery('.volume-ditunda-kemhan').html(jQuery.formatNumber(value.tunda_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_total>0){
					jQuery('.rupiah-ditunda-kemhan').html(jQuery.formatNumber(value.tunda_total, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_st_pakai>0){
					jQuery('.volume-ditolak-kemhan').html(jQuery.formatNumber(value.tolak_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_total>0){
					jQuery('.rupiah-ditolak-kemhan').html(jQuery.formatNumber(value.tolak_total, {format:'#,###', locale:'us'}));
				}
				// volume dan rupiah yg disetujui
				jQuery('.volume-disetujui-kemhan').html(jQuery.formatNumber((parseInt(value.klarifikasi_st_pakai)+parseInt(value.rek_st_pakai))-(parseInt(value.tunda_st_pakai)+parseInt(value.tolak_st_pakai)), {format:'#,###', locale:'us'}));
				jQuery('.rupiah-disetujui-kemhan').html(jQuery.formatNumber((parseInt(value.klarifikasi_total) + parseInt(value.rek_total))-(value.tunda_total + parseInt(value.tolak_total)), {format:'#,###', locale:'us'}));
				break;
			case '010201':
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-diklarifikasi-mabes').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-diklarifikasi-mabes').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}
				if(value.rek_st_pakai>0){
					jQuery('.volume-diajukan-mabes').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-diajukan-mabes').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_st_pakai>0){
					jQuery('.volume-ditunda-mabes').html(jQuery.formatNumber(value.tunda_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_total>0){
					jQuery('.rupiah-ditunda-mabes').html(jQuery.formatNumber(value.tunda_total, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_st_pakai>0){
					jQuery('.volume-ditolak-mabes').html(jQuery.formatNumber(value.tolak_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_total>0){
					jQuery('.rupiah-ditolak-mabes').html(jQuery.formatNumber(value.tolak_total, {format:'#,###', locale:'us'}));
				}
				// volume dan rupiah yg disetujui
				jQuery('.volume-disetujui-mabes').html(jQuery.formatNumber((parseInt(value.klarifikasi_st_pakai)+parseInt(value.rek_st_pakai))-(parseInt(value.tunda_st_pakai)+parseInt(value.tolak_st_pakai)), {format:'#,###', locale:'us'}));
				jQuery('.rupiah-disetujui-mabes').html(jQuery.formatNumber((parseInt(value.klarifikasi_total) + parseInt(value.rek_total))-(parseInt(value.tunda_total) + parseInt(value.tolak_total)), {format:'#,###', locale:'us'}));
				break;
			case '010300':
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-diklarifikasi-ad').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-diklarifikasi-ad').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}
				if(value.rek_st_pakai>0){
					jQuery('.volume-diajukan-ad').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-diajukan-ad').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_st_pakai>0){
					jQuery('.volume-ditunda-ad').html(jQuery.formatNumber(value.tunda_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_total>0){
					jQuery('.rupiah-ditunda-ad').html(jQuery.formatNumber(value.tunda_total, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_st_pakai>0){
					jQuery('.volume-ditolak-ad').html(jQuery.formatNumber(value.tolak_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_total>0){
					jQuery('.rupiah-ditolak-ad').html(jQuery.formatNumber(value.tolak_total, {format:'#,###', locale:'us'}));
				}
				// volume dan rupiah yg disetujui
				jQuery('.volume-disetujui-ad').html(jQuery.formatNumber((parseInt(value.klarifikasi_st_pakai) + parseInt(value.rek_st_pakai)) - (parseInt(value.tunda_st_pakai) + parseInt(value.tolak_st_pakai)), {format:'#,###', locale:'us'}));
				jQuery('.rupiah-disetujui-ad').html(jQuery.formatNumber((parseInt(value.klarifikasi_total) + parseInt(value.rek_total)) - (parseInt(value.tunda_total) + parseInt(value.tolak_total)), {format:'#,###', locale:'us'}));
				break;
			case '010400':
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-diklarifikasi-al').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-diklarifikasi-al').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}
				if(value.rek_st_pakai>0){
					jQuery('.volume-diajukan-al').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-diajukan-al').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_st_pakai>0){
					jQuery('.volume-ditunda-al').html(jQuery.formatNumber(value.tunda_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_total>0){
					jQuery('.rupiah-ditunda-al').html(jQuery.formatNumber(value.tunda_total, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_st_pakai>0){
					jQuery('.volume-ditolak-al').html(jQuery.formatNumber(value.tolak_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_total>0){
					jQuery('.rupiah-ditolak-al').html(jQuery.formatNumber(value.tolak_total, {format:'#,###', locale:'us'}));
				}
				// volume dan rupiah yg disetujui
				jQuery('.volume-disetujui-al').html(jQuery.formatNumber((parseInt(value.klarifikasi_st_pakai)+parseInt(value.rek_st_pakai))-(parseInt(value.tunda_st_pakai)+parseInt(value.tolak_st_pakai)), {format:'#,###', locale:'us'}));
				jQuery('.rupiah-disetujui-al').html(jQuery.formatNumber((value.klarifikasi_total + parseInt(value.rek_total))-(value.tunda_total + parseInt(value.tolak_total)), {format:'#,###', locale:'us'}));
				break;
			case '010500':
				if(value.klarifikasi_st_pakai>0){
					jQuery('.volume-diklarifikasi-au').html(jQuery.formatNumber(value.klarifikasi_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.klarifikasi_total>0){
					jQuery('.rupiah-diklarifikasi-au').html(jQuery.formatNumber(value.klarifikasi_total, {format:'#,###', locale:'us'}));
				}
				if(value.rek_st_pakai>0){
					jQuery('.volume-diajukan-au').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.rek_total>0){
					jQuery('.rupiah-diajukan-au').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_st_pakai>0){
					jQuery('.volume-ditunda-au').html(jQuery.formatNumber(value.tunda_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tunda_total>0){
					jQuery('.rupiah-ditunda-au').html(jQuery.formatNumber(value.tunda_total, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_st_pakai>0){
					jQuery('.volume-ditolak-au').html(jQuery.formatNumber(value.tolak_st_pakai, {format:'#,###', locale:'us'}));
				}
				if(value.tolak_total>0){
					jQuery('.rupiah-ditolak-au').html(jQuery.formatNumber(value.tolak_total, {format:'#,###', locale:'us'}));
				}
				// volume dan rupiah yg disetujui
				jQuery('.volume-disetujui-au').html(jQuery.formatNumber((parseInt(value.klarifikasi_st_pakai)+parseInt(value.rek_st_pakai))-(parseInt(value.tunda_st_pakai)+parseInt(value.tolak_st_pakai)), {format:'#,###', locale:'us'}));
				jQuery('.rupiah-disetujui-au').html(jQuery.formatNumber((value.klarifikasi_total + parseInt(value.rek_total))-(value.tunda_total + parseInt(value.tolak_total)), {format:'#,###', locale:'us'}));
				break;
			default:
		}
		// total diklarifikasi
		totalVolDiklarifikasi = totalVolDiklarifikasi + parseInt(value.klarifikasi_st_pakai);
		totalRupDiklarifikasi = totalRupDiklarifikasi + parseInt(value.klarifikasi_total);
		if(totalVolDiklarifikasi>0){
			jQuery('.volume-diklarifikasi-total').html(jQuery.formatNumber(totalVolDiklarifikasi, {format:'#,###', locale:'us'}));
		}
		if(totalRupDiklarifikasi>0){
			jQuery('.rupiah-diklarifikasi-total').html(jQuery.formatNumber(totalRupDiklarifikasi, {format:'#,###', locale:'us'}));
		}
		// total diajukan
		totalVolDiajukan = totalVolDiajukan + parseInt(value.rek_st_pakai);
		totalRupDiajukan = totalRupDiajukan + parseInt(value.rek_total);
		if(totalVolDiajukan>0){
			jQuery('.volume-diajukan-total').html(jQuery.formatNumber(totalVolDiajukan, {format:'#,###', locale:'us'}));
		}
		if(totalRupDiajukan>0){
			jQuery('.rupiah-diajukan-total').html(jQuery.formatNumber(totalRupDiajukan, {format:'#,###', locale:'us'}));
		}
		// total ditunda
		totalVolDitunda = totalVolDitunda + parseInt(value.tunda_st_pakai);
		totalRupDitunda = totalRupDitunda + parseInt(value.tunda_total);
		if(totalVolDitunda>0){
			jQuery('.volume-ditunda-total').html(jQuery.formatNumber(totalVolDitunda, {format:'#,###', locale:'us'}));
		}
		if(totalRupDitunda>0){
			jQuery('.rupiah-ditunda-total').html(jQuery.formatNumber(totalRupDitunda, {format:'#,###', locale:'us'}));
		}
		// total ditolak
		totalVolDitolak = totalVolDitolak + parseInt(value.tolak_st_pakai);
		totalRupDitolak = totalRupDitolak + parseInt(value.tolak_total);
		if(totalVolDitolak>0){
			jQuery('.volume-ditolak-total').html(jQuery.formatNumber(totalVolDitolak, {format:'#,###', locale:'us'}));
		}
		if(totalRupDitolak>0){
			jQuery('.rupiah-ditolak-total').html(jQuery.formatNumber(totalRupDitolak, {format:'#,###', locale:'us'}));
		}
	});

	// kesimpulan
	jQuery('.volume-disetujui-total').html(jQuery.formatNumber((totalVolDiklarifikasi + totalVolDiajukan) - (totalVolDitunda + totalVolDitolak), {format:'#,###', locale:'us'}));
	jQuery('.rupiah-disetujui-total').html(jQuery.formatNumber((totalRupDiklarifikasi + totalRupDiajukan) - (totalRupDitunda + totalRupDitolak), {format:'#,###', locale:'us'}));
	jQuery('.rupiah-disetujui-bulat').html(jQuery.formatNumber((Math.ceil(((totalRupDiklarifikasi + totalRupDiajukan) - (totalRupDitunda + totalRupDitolak))/1000)*1000), {format:'#,###', locale:'us'}));
	// conversi angka ke string
	targetUrl	= "/api/perpamsi/referensi/num_to_string.php";
	jQuery.post(targetUrl, {filter: [{name: 'rupiah', value: (Math.ceil(((totalRupDiklarifikasi + totalRupDiajukan) - (totalRupDitunda + totalRupDitolak))/1000)*1000)}]}, function(data){
		jQuery('.string-pembulatan').html(data.string);
	}, 'json');
}, 'json');
