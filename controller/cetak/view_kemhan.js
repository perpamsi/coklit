// halaman berita acara : front
// halaman rincian tagihan : front
kunci		= new Date();
var bulan	= kunci.getMonth() + 1;
if(bulan<10){
	bulan	= '0' + bulan;
}

jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// halaman berita acara
		if(dataTemp.applKode=='070101'){
			dataTemp.proses[value.gp_kode]	= {proses: 'cetakBA', title: value.gp_nama, filter: [{name: 'bulan', value: bulan}, {name: 'tahun', value: kunci.getFullYear()}, {name: 'gp_kode', value: value.gp_kode}]};
		}
		else{
			dataTemp.proses[value.gp_kode]	= {proses: 'cetakRinci', title: value.gp_nama, filter: [{name: 'bulan', value: bulan}, {name: 'tahun', value: kunci.getFullYear()}, {name: 'gp_kode', value: value.gp_kode}]};
		}

		jQuery('#tabel-kemhan-cetak tbody').append('<tr onclick="loadFile(\'' + value.gp_kode + '\')" style=\"cursor: pointer\"><td>' + value.gp_nama + '</td></tr>');
	});

	// define proses kembali
	dataTemp.proses.kembali			= {proses: dataTemp.applKode};

	// define proses cetak ba rekap
	dataTemp.proses.cetakPengantar	= {proses: 'cetakPengantar', filter: [{name: 'bulan', value: bulan}, {name: 'tahun', value: kunci.getFullYear()}, {name: 'gp_kode', value: '010000'}]};

	// define proses cetak ba rekap
	dataTemp.proses.cetakBASeluruh	= {proses: 'cetakBASeluruh', filter: [{name: 'bulan', value: bulan}, {name: 'tahun', value: kunci.getFullYear()}, {name: 'gp_kode', value: '010000'}]};

	// define proses cetak drd rekap
	dataTemp.proses.cetakDRDRekap	= {proses: 'cetakDRDRekap', filter: [{name: 'bulan', value: bulan}, {name: 'tahun', value: kunci.getFullYear()}, {name: 'gp_kode', value: '010000'}]};

	// define proses rincian coklit
	dataTemp.proses.cetakDRDRinci	= {proses: 'cetakDRDRinci', filter: [{name: 'bulan', value: bulan}, {name: 'tahun', value: kunci.getFullYear()}, {name: 'gp_kode', value: '010000'}]};

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery('#tabel-kemhan-cetak').dataTable({
		stateSave: true
	});

}, "json");

function pilihBulan(param){
	jQuery.each(dataTemp.proses, function(i){
		if(typeof(dataTemp.proses[i].filter)=='object'){
			dataTemp.proses[i].filter[0].value = param;
		}
	});

	// update penanda bulan
	kunci	= namaBulan(param) + ' <span class="caret"></span>';
	jQuery('#inputBulan').html(kunci);

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
}

function pilihTahun(param){
	jQuery.each(dataTemp.proses, function(i){
		if(typeof(dataTemp.proses[i].filter)=='object'){
			dataTemp.proses[i].filter[1].value = param;
		}
	});
	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
}
