jQuery.get(targetHtm, function(inHTML){
	jQuery('.block').html(inHTML);

	// retrieve data
	jQuery.post(targetUrl, {}, function(data){
		jQuery.each(data,function(i,value){
			// breadcrumb
			inHTML	= '<li onclick="loadMenu(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>';
			// defining kontrol proses
			dataTemp.proses[value.dpd_kode]	= {proses: 'rekapPDAM', breadcrumb: inHTML, title: value.dpd_nama, filter: [{name: 'dpd_kode', value: value.dpd_kode}]};

			jQuery('table tbody').append('<tr onclick="loadFile(\'' + value.dpd_kode + '\')" style="cursor: pointer"><td>' + value.dpd_nama+ '</td></tr>');
		});
		
		// define proses kembali
		dataTemp.proses.kembali			= {proses: dataTemp.applKode};
		
		// storing kontrol proses
		localStorage.setItem('perpamsi', JSON.stringify(dataTemp));

		jQuery(document).ready(function(){
			jQuery('table').dataTable({
				stateSave: true
			});
		});

	}, "json");
});
