// halaman cetak drd : print

// unit organisasi
if(typeof(dataFeed.title)=='string'){
	jQuery('.unit-organisasi').html(dataFeed.title);
}
// tagihan bulan ini
var bulan	= parseInt(dataFeed.filter[0].value);
var tahun	= dataFeed.filter[1].value;
inHTML		= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-tagihan').html(inHTML);
// rekening bulan ini
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-1').html(inHTML);
// rekening bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
if(bulan>2){
	inHTML	= namaBulan(bulan);
}
else{
	inHTML	= namaBulan(bulan) + ' ' + tahun;
}
jQuery('.bulan-rekening-2').html(inHTML);
// rekening 2 bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-3').html(inHTML);

// ambil data referensi
targetUrl	= "/api/perpamsi/referensi/view_reff.php";
dataFeed.filter.push({name: 'tt', value: 'BA' + dataFeed.filter[2].value});
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		jQuery('span.' + value.tc).html(value.tdesc);
		jQuery('input.' + value.tc + '[name=tdesc]').val(value.tdesc);
	});
}, 'json');

// ambil data rekening
targetUrl	= "/api/perpamsi/kemhan/kotama/satuan/pdam/view_rekap.php";
jQuery.post(targetUrl, dataFeed, function(data){
	var no 				= 0;
	var totRekening 	= 0;
	var totKlarifikasi	= 0;
	var totVolume05		= 0;
	var totRupiah05		= 0;
	var totVolume01		= 0;
	var totRupiah01		= 0;
	var totVolume00		= 0;
	var totRupiah00		= 0;
	var totVolume		= 0;
	var totRupiah		= 0;
	jQuery.each(data, function(i, value){
		var jmlRek 				= 0;
		var rupiahKlarifikasi	= 0;
		var volume05			= 0;
		var rupiah05			= 0;
		var volume01			= 0;
		var rupiah01			= 0;
		var volume00			= 0;
		var rupiah00			= 0;
		var volume				= 0;
		var rupiah				= 0;
		var inHTML				= '';
		var kotama				= '';
		var pdam_kode			= value[0].pdam_kode;
		jQuery.each(value, function(j, param){
			if(pdam_kode != param.pdam_kode || j == 0){
				no 			= no + 1;
				nomer		= no;
				pdam_nama	= param.pdam_nama;
			}
			else{
				nomer		= '';
				pdam_nama	= '';
			}
			// pdam atau satuan kerja
			inHTML	= inHTML + '<tr>' +
						'<td align="right">' + nomer + '</td>' +
						'<td>' + pdam_nama + '</td>' +
						'<td>' + param.pel_nama + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_lembar, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.klarifikasi_total, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_st_pakai05, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_total05, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_st_pakai01, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_total01, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_st_pakai00, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_total00, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(param.rek_total, {format:'#,###', locale:'en'}) + '</td>' +
					'</tr>';
			// jumlah rekening
			jmlRek 				= jmlRek + parseInt(param.rek_lembar);
			rupiahKlarifikasi	= rupiahKlarifikasi + parseInt(param.klarifikasi_total);
			volume05			= volume05 + parseInt(param.rek_st_pakai05);
			rupiah05			= rupiah05 + parseInt(param.rek_total05);
			volume01			= volume01 + parseInt(param.rek_st_pakai01);
			rupiah01			= rupiah01 + parseInt(param.rek_total01);
			volume00			= volume00 + parseInt(param.rek_st_pakai00);
			rupiah00			= rupiah00 + parseInt(param.rek_total00);
			volume				= volume + parseInt(param.rek_st_pakai);
			rupiah				= rupiah + parseInt(param.rek_total);
			pdam_kode			= param.pdam_kode;
			// jumlah seluruhnya
			totRekening			= totRekening + parseInt(param.rek_lembar);
			totKlarifikasi		= totKlarifikasi + parseInt(param.klarifikasi_total);
			totVolume05			= totVolume05 + parseInt(param.rek_st_pakai05);
			totRupiah05			= totRupiah05 + parseInt(param.rek_total05);
			totVolume01			= totVolume01 + parseInt(param.rek_st_pakai01);
			totRupiah01			= totRupiah01 + parseInt(param.rek_total01);
			totVolume00			= totVolume00 + parseInt(param.rek_st_pakai00);
			totRupiah00			= totRupiah00 + parseInt(param.rek_total00);
			totVolume			= totVolume + parseInt(param.rek_st_pakai);
			totRupiah			= totRupiah + parseInt(param.rek_total);
		});
		// kotama
		kotama	= '<tr style="font-weight: bold">' +
					'<td></td>' +
					'<td>' + value[0].gp_nama +'</td>' +
					'<td></td>' +
					'<td align="right">' + jQuery.formatNumber(jmlRek, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(rupiahKlarifikasi, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(volume05, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(rupiah05, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(volume01, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(rupiah01, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(volume00, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(rupiah00, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(volume, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(rupiah, {format:'#,###', locale:'en'}) + '</td>' +
				'</tr>';
		// kotama
		jQuery('#tabel-kemhan-cetak tbody').append(kotama);
		// satuan
		jQuery('#tabel-kemhan-cetak tbody').append(inHTML);
	});
	// jumlah seluruhnya
	jQuery('.rekening').html(jQuery.formatNumber(totRekening, {format:'#,###', locale:'en'}));
	jQuery('.klarifikasi').html(jQuery.formatNumber(totKlarifikasi, {format:'#,###', locale:'en'}));
	jQuery('.volume05').html(jQuery.formatNumber(totVolume05, {format:'#,###', locale:'en'}));
	jQuery('.rupiah05').html(jQuery.formatNumber(totRupiah05, {format:'#,###', locale:'en'}));
	jQuery('.volume01').html(jQuery.formatNumber(totVolume01, {format:'#,###', locale:'en'}));
	jQuery('.rupiah01').html(jQuery.formatNumber(totRupiah01, {format:'#,###', locale:'en'}));
	jQuery('.volume00').html(jQuery.formatNumber(totVolume00, {format:'#,###', locale:'en'}));
	jQuery('.rupiah00').html(jQuery.formatNumber(totRupiah00, {format:'#,###', locale:'en'}));
	jQuery('.volume').html(jQuery.formatNumber(totVolume, {format:'#,###', locale:'en'}));
	jQuery('.rupiah').html(jQuery.formatNumber(totRupiah, {format:'#,###', locale:'en'}));
}, 'json');

// define proses kembali ke halaman unit organisasi
if(dataFeed.filter[2].value!='010000'){
	dataTemp.proses.kembali	= dataTemp.proses[dataFeed.filter[2].value];
}
// storing kontrol proses
localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
