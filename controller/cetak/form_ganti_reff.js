// modal form
jQuery('#' + dataFeed.targetId).modal('show');
jQuery('.modal-title').html(dataFeed.title);
jQuery('input.simpan[name=tdesc]').prop('value', dataFeed.tdesc);

// event tutup modal
jQuery('#button-tutup').click(function(){
	jQuery('#' + dataFeed.targetId).modal('hide');
	jQuery('#' + dataFeed.targetId).remove();
});

// event simpan referensi
jQuery('#button-simpan').click(function(){
	dataFeed.data 	= jQuery('input.simpan').serializeArray();
	jQuery('span.' + dataFeed.tc).html(dataFeed.data[0].value);
	dataFeed.data.push({name: 'tc', value: dataFeed.filter[1].value.toString() + dataFeed.filter[0].value + dataFeed.tc});
	dataFeed.data.push(dataFeed.filter[3]);
	if(dataFeed.data[0].value.length>0){
		jQuery('#button-simpan').prop('disabled', true);
		targetUrl	= "/api/perpamsi/referensi/sunting_reff.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
		}, "json");
	}
	else{
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass('alert alert-warning');
		jQuery('#pesan div').html('Data referensi belum diisi');
	}
});
