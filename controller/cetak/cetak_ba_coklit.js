// halaman cetak berita acara : print

// unit organisasi
jQuery('.unit-organisasi').html(dataFeed.title);

// tagihan bulan ini
var bulan	= parseInt(dataFeed.filter[0].value);
var tahun	= dataFeed.filter[1].value;
inHTML		= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-tagihan').html(inHTML);
// rekening bulan ini
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-1').html(inHTML);
// rekening bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
if(bulan>1){
	inHTML	= namaBulan(bulan);
}
else{
	inHTML	= namaBulan(bulan) + ' ' + tahun;
}
jQuery('.bulan-rekening-2').html(inHTML);
// rekening 2 bulan lalu
if(bulan==1){
	bulan = 12;
	tahun = tahun - 1;
}
else{
	bulan = bulan - 1;
}
inHTML	= namaBulan(bulan) + ' ' + tahun;
jQuery('.bulan-rekening-3').html(inHTML);

// define proses rincian coklit
dataTemp.proses.cetakDRDRinci	= {proses: 'cetakDRDRinci', title: dataFeed.title, filter: dataFeed.filter};
// define proses tunda coklit
dataTemp.proses.cetakDRDTunda	= {proses: 'cetakDRDTunda', title: dataFeed.title, filter: dataFeed.filter};
// define proses kembali ke halaman kemhan
dataTemp.proses.kembali			= {proses: dataTemp.applKode};
// storing kontrol proses
localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

// delete data refernsi KEMHAN, MABES, AU
if(dataFeed.filter[2].value=="010101" || dataFeed.filter[2].value=="010201" || dataFeed.filter[2].value=="010500"){
	jQuery('#removeForm').remove();
}

// ambil data referensi
targetUrl	= "/api/perpamsi/referensi/view_reff.php";
dataFeed.filter.push({name: 'tt', value: 'BA' + dataFeed.filter[2].value});
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		jQuery('span.' + value.tc).html(value.tdesc);
		jQuery('input.' + value.tc + '[name=tdesc]').val(value.tdesc);
	});
}, 'json');

// ambil data rekening
targetUrl	= "/api/perpamsi/rekening/view_rekap.php";
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		jQuery('.volume-diajukan').html(jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}));
		jQuery('.rupiah-diajukan').html(jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}));
		jQuery('.volume-ditunda').html(jQuery.formatNumber(value.tunda_st_pakai, {format:'#,###', locale:'us'}));
		jQuery('.rupiah-ditunda').html(jQuery.formatNumber(value.tunda_total, {format:'#,###', locale:'us'}));
		jQuery('.volume-ditolak').html(jQuery.formatNumber(value.tolak_st_pakai, {format:'#,###', locale:'us'}));
		jQuery('.rupiah-ditolak').html(jQuery.formatNumber(value.tolak_total, {format:'#,###', locale:'us'}));
		jQuery('.volume-disetujui').html(jQuery.formatNumber((value.rek_st_pakai-value.tunda_st_pakai-value.tolak_st_pakai), {format:'#,###', locale:'us'}));
		jQuery('.rupiah-disetujui').html(jQuery.formatNumber((value.rek_total-value.tunda_total-value.tolak_total), {format:'#,###', locale:'us'}));
		jQuery('.rupiah-pembulatan').html(jQuery.formatNumber((Math.ceil((value.rek_total-value.tunda_total-value.tolak_total)/1000)*1000), {format:'#,###', locale:'us'}));

		// conversi angka ke string
		targetUrl	= "/api/perpamsi/referensi/num_to_string.php";
		jQuery.post(targetUrl, {filter: [{name: 'rupiah', value: (Math.ceil((value.rek_total-value.tunda_total-value.tolak_total)/1000)*1000)}]}, function(data){
			jQuery('.string-pembulatan').html(data.string);
		}, 'json');
	});
}, 'json');
