// halaman laporan perpamsi : front
// breadcrumb
inHTML	= '<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>';
if(jQuery('.breadcrumb').hasClass('hide')){
	jQuery('.breadcrumb').removeClass('hide');
	jQuery('.breadcrumb').html(inHTML);
}

jQuery.post(targetUrl, {}, function(data){
	jQuery.each(data,function(i,value){
		// breadcrumb
		inHTML	= '<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
				'<li onclick="loadFile(\'' + value.dpd_kode + '\')" style=\"cursor: pointer\">' + value.dpd_nama + '</li>';
		// defining kontrol proses
		dataTemp.proses[value.dpd_kode]	= {proses: 'rekapDpd', title: value.dpd_nama, breadcrumb: inHTML, filter: [{name: 'dpd_kode', value: value.dpd_kode}]};

		inHTML	= '<tr onclick="loadFile(\'' + value.dpd_kode + '\')" style=\"cursor: pointer\"><td>' + value.dpd_nama+ '</td>' +
				'<td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'us'}) + '</td>' +
				'<td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}) + '</td>' +
				'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}) + '</td>' +
			'</tr>';
		jQuery('#tabel-dpd-laporan tbody').append(inHTML);
	});

	// define proses awal menu
	dataTemp.proses[dataTemp.applKode]	= {proses: dataTemp.applKode};
	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	jQuery(document).ready(function(){
		jQuery('#tabel-dpd-laporan').dataTable({
			stateSave: true
		});
	});

}, "json");
