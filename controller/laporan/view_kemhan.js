jQuery.post(targetUrl, dataFeed, function(data){
	var totalLembar	= 0;
	var totalVolume	= 0;
	var totalRupiah	= 0;
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		inHTML	=	'<li onclick="loadMenu(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li>' + value.gp_nama + '</li>';
		// halaman laporan perpamsi
		if(dataFeed.proses=='rekapPdamKotama'){
			dataTemp.proses[value.gp_kode]	= {proses: 'rekapPdamSatuan', title: dataFeed.title + ' <i class="fa fa-angle-double-right"></i> ' + value.gp_nama, filter: [dataFeed.filter[0], {name: "gp_parent", value: value.gp_parent}]};
		}
		else{
			dataTemp.proses[value.gp_parent]	= {proses: 'rekapKotama', breadcrumb: inHTML, filter: [{name: "gp_parent", value: value.gp_parent}]};
		}

		totalLembar = totalLembar + parseInt(value.rek_lembar);
		totalVolume = totalVolume + parseInt(value.rek_st_pakai);
		totalRupiah = totalRupiah + parseInt(value.rek_total);

		jQuery('#tabel-rekap-kemhan tbody').append('<tr onclick="loadFile(\'' + value.gp_parent + '\')" style=\"cursor: pointer\"><td>' + value.gp_nama + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'us'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}) + '</td></tr>');
	});

	inHTML	= '<tr>' +
				'<td>&nbsp;</td>' +
				'<td align="right"><strong>' + jQuery.formatNumber(totalLembar, {format:'#,###', locale:'us'}) + '</strong></td>' +
				'<td align="right"><strong>' + jQuery.formatNumber(totalVolume, {format:'#,###', locale:'us'}) + '</strong></td>' +
				'<td align="right"><strong>' + jQuery.formatNumber(totalRupiah, {format:'#,###', locale:'us'}) + '</strong></td>' +
			'</tr>';
	jQuery('#tabel-rekap-kemhan tfoot').html(inHTML);

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	jQuery(document).ready(function(){
		jQuery('#tabel-rekap-kemhan').dataTable({
			stateSave: true
		});
	});

}, "json");
