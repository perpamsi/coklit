jQuery('.head-1').remove();
jQuery('.head-3').remove();
jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery.each(data,function(i,value){
		if(value.rek_lembar>0){
			// halaman laporan perpamsi
			if(dataFeed.proses=='rekapPdamSatuan'){
				dataTemp.proses[value.pel_no]	= {proses: 'rinciPdamSatuan', title: dataFeed.title + ' <i class="fa fa-angle-double-right"></i> ' + value.pel_nama, filter: [dataFeed.filter[0], {name: 'pel_no', value: value.pel_no}]};
			}
			// halaman laporan kemhan
			else{
				dataTemp.proses[value.pel_no]	= {proses: 'rinciSatuan', title: dataFeed.title + ' <i class="fa fa-angle-double-right"></i> ' + value.pel_nama, filter: [{name: 'pel_no', value: value.pel_no}]};
			}

			jQuery('#tabel-rekap-satuan tbody').append('<tr><td>' + value.pel_nama + '</td><td>' + value.pdam_nama + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td></tr>');
		}
	});

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// breadcrumb
	if(jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').removeClass('hide');
	}
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// rendering tabel
	jQuery(document).ready(function() {
		jQuery('#tabel-rekap-satuan').dataTable( {
			stateSave: true
		});
	});
}, "json");
