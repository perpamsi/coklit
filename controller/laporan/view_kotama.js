// halaman laporan kemhan : level 2
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		inHTML	=	'<li onclick="loadMenu(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'' + value.gp_parent + '\')" style=\"cursor: pointer\">' + value.kotama + '</li>' +
					'<li>' + value.gp_nama + '</li>';

		dataTemp.proses[value.gp_kode]	= {proses: "rekapSatuan", breadcrumb: inHTML, filter: [{name: "gp_kode", value: value.gp_kode}]};

		jQuery('#tabel-rekap-kotama tbody').append('<tr onclick="loadFile(\'' + value.gp_kode + '\')" style=\"cursor: pointer\"><td>' + value.gp_roman + '</td><td>' + value.gp_nama + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'de'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'de'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'de'}) + '</td></tr>');
	});

	// storing control process
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// breadcrumb
	if(jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').removeClass('hide');
	}
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	jQuery(document).ready(function(){
		jQuery('#tabel-rekap-kotama').dataTable({
			stateSave: true
		});
	});

}, "json");
