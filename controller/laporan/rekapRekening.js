// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

jQuery.get(targetHtm, function(inHTML){
	jQuery('.block').html(inHTML);

	// retrieve data
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery.each(data,function(i,value){
			// kontrol proses kembali
			kunci	= value.dpd_kode;
			// breadcrumb
			inHTML	= dataFeed.breadcrumb +
					'<li onclick="loadFile(\'' + value.rek_nomor + '\')" style="cursor: pointer">' + 'Rekening ' + value.bln_rekening + '</li>';
			// kontrol proses
			dataTemp.proses[value.rek_nomor]	= {proses: "formApprove", breadcrumb: inHTML, title: value.pel_nama, filter: [{name: 'rek_nomor', value: value.rek_nomor}]};
			inHTML	= '<tr onclick="loadFile(\'' + value.rek_nomor + '\')" style="cursor: pointer">' +
					'<td>' + (i + 1) + '</td>' +
					'<td>' + value.pel_nama + '</td>' +
					'<td>' + value.bln_rekening + '</td>' +
					'<td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td>' +
					'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td>' +
					'<td>' + value.status + '</td>' +
				'</tr>';
			jQuery('.table-responsive table tbody').append(inHTML);
		});

		// rendering tabel
		 jQuery(document).ready(function() {
			 jQuery('.table-responsive table').dataTable( {
				 stateSave: true
			 });
		 });

		// append button kembali
		inHTML	= '<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')"><i class="fa fa-arrow-circle-left"></i> Kembali</button>' +
			'<div class="clear-fix"></div>';
		jQuery('.block').append(inHTML);

		// storing kontrol proses
		dataTemp.proses.kembali	= dataTemp.proses[kunci];
		localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
	}, 'json');
});
