// halaman laporan kemhan : level4
// halaman laporan perpamsi : level5
// halaman laporan pdam : level2

jQuery('.block-title h2').html(dataFeed.title);
// halaman laporan pdam
if(dataFeed.proses=='rinciRekening'){
	jQuery('#tabel-rekening-laporan thead tr').prepend('<th>Satuan Tugas</th>');
	jQuery('#tabel-rekening-laporan tfoot tr').prepend('<th>Satuan Tugas</th>');
}
jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery.each(data,function(i,value){
		// halaman laporan pdam
		if(value.rek_lembar>0){
			if(dataFeed.proses=='rinciRekening'){
				jQuery('#tabel-rekening-laporan tbody').append('<tr><td>' + value.pel_nama + '</td><td>' + value.bln_coklit + '</td><td align="center">' + value.bln_tagihan + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td><td>Disetujui</td></tr>');
			}
			else{
				jQuery('#tabel-rekening-laporan tbody').append('<tr><td>' + value.bln_coklit + '</td><td align="center">' + value.bln_tagihan + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td><td>Disetujui</td></tr>');
			}
		}
	});

	jQuery('#button-kembali').click(function(){
		// halaman laporan perpamsi
		if(dataFeed.proses=='rinciPdamSatuan'){
			loadFile('level4');
		}
		// halaman laporan kemhan
		else if(dataFeed.proses=='rinciSatuan'){
			loadFile('level3');
		}
		// halaman laporan pdam
		else{
			loadMenu(dataTemp.applKode);
		}
	});

	// rendering tabel
	 jQuery(document).ready(function() {
		 jQuery('#tabel-rekening-laporan').dataTable( {
			 stateSave: true
		 });
	 });
}, "json");
