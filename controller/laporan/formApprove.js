// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

jQuery.get(targetHtm, function(inHTML){
	jQuery('.block').html(inHTML);

	// title
	jQuery('.block-title').removeClass('hidden');
	jQuery('.block-title h2').html(dataFeed.title);

	// manipulasi form
	jQuery('input').prop('disabled', true);

	// retrieve data
	jQuery.post(targetUrl, dataFeed, function(data){	
		jQuery.each(data,function(i,value){
			jQuery('input[name=input_tanggal]').attr('value', value.input_tanggal);
			jQuery('input[name=input_nama]').attr('value', value.input_nama);
			jQuery('input[name=input_priv]').attr('value', value.input_priv);
			jQuery('input[name=rek_lembar]').attr('value', jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}));
			jQuery('input[name=rek_st_pakai]').attr('value', jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}));
			jQuery('input[name=rek_total]').attr('value', jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}));
			jQuery('input[name=status]').attr('value', value.status);

			// storing kontrol proses
			dataTemp.proses.kembali	= dataTemp.proses[value.pdam_kode];
			localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
		});
	}, 'json');
});
