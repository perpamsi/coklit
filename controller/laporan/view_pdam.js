// halaman laporan perpamsi : level 2
// halaman laporan pdam : front
if(typeof dataTemp.procKode == 'string'){
	jQuery('.block-title h2').html(dataFeed.title);
	kunci		= 'rekapPdamKotama';
}
else{
	kunci		= 'rinciRekening';
}

// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery.each(data,function(i,value){
		// define kontrol proses
		dataTemp.proses[dataTemp.applKode]	= {proses: dataTemp.applKode};
		inHTML	=	'<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'' + value.dpd_kode + '\')" style="cursor: pointer">' + dataFeed.title + '</li>' +
					'<li onclick="loadFile(\'' + value.pdam_kode + '\')" style="cursor: pointer">' + value.pdam_nama + '</li>';
		dataTemp.proses[value.pdam_kode]	= {proses: 'rekapRekening', title: value.pdam_nama, breadcrumb: inHTML, filter: [{name: 'pdam_kode', value: value.pdam_kode}]};

		jQuery('#tabel-pdam-laporan tbody').append('<tr onclick="loadFile(\'' + value.pdam_kode + '\')" style="cursor: pointer"><td>' + value.pdam_nama + '</td><td>' + value.pdam_kota + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'us'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}) + '</td></tr>');
	});

	// storing kontrol proses
	dataTemp.proses.level2	= dataFeed;
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// set tombol kembali ke menu sebelumnya
	jQuery('#button-kembali').click(function(){
		loadMenu(dataTemp.applKode);
	});

	// rendering tabel
	jQuery(document).ready(function() {
		jQuery('#tabel-pdam-laporan').dataTable( {
			stateSave: true
		});
	});
}, 'json');
