// inquiry data PDAM
var targetUrl   = "/api/perpamsi/get_rekap_tunda.php";
var dataProc	= {};
jQuery.post(targetUrl, {}, function(data) {
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataProc['rinci_' + value.pdam_kode]	= {proses: "rinci_" + dataTemp.applKode, pdamNama: value.pdam_nama, filter: [{field: "pdam_kode", value: value.pdam_kode}]};
		
		jQuery('#tabel-pdam tbody').append('<tr onclick="loadFile(\'rinci_' + value.pdam_kode + '\')" style=\"cursor: pointer\"><td>' + value.pdam_kode + '</td><td>' + value.pdam_nama + '</td><td>' + value.pdam_kota + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'de'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'de'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'de'}) + '</td></tr>');
	});

	// storing kontrol proses
	jQuery.extend(dataTemp, {proses: dataProc});
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
	
	// rendering tabel
	jQuery(document).ready(function() {
		jQuery('#tabel-pdam').dataTable( {
			stateSave: true
		} );
	} );
}, "json");
