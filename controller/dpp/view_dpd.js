dataProc	= {};
targetUrl   = "/api/perpamsi/get_dpd.php";
jQuery.post(targetUrl, {}, function(data){
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataProc['rinci_' + value.dpd_kode]	= {proses: "rinci_" + dataTemp.applKode, dpdKode: value.dpd_kode, dpdNama: value.dpd_nama, filter: [{field: "dpd_kode", value: value.dpd_kode}]};

		jQuery('#tabel-dpd tbody').append('<tr onclick="loadFile(\'rinci_' + value.dpd_kode + '\')" style=\"cursor: pointer\"><td>' + value.dpd_kode + '</td><td>' + value.dpd_nama+ '</td></tr>');
	});

	// storing kontrol proses
	dataProc['tambah']  = {proses: "tambah_dpd_" + dataTemp.applKode};
	dataProc['kembali']	= {proses: dataTemp.applKode};
	jQuery.extend(dataTemp, {proses: dataProc});
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	jQuery('#button-tambah').click(function(){
		loadFile('tambah');
	});

	jQuery(document).ready(function(){
		jQuery('#tabel-dpd').dataTable({
			stateSave: true
		});
	});

}, "json");
