
jQuery('select').prop('disabled', true);
jQuery('input').prop('disabled', true);

// retrieve informasi dpd
kunci	 = dataTemp.proses[dataTemp.procKode];
dataFeed = {filter: [{name: "dpd_kode", value: kunci.dpdKode }]};
jQuery('.block-title h2').html(kunci.dpdNama);
targetUrl	= "http://localhost/api/perpamsi/dpp/get_dpd.php";
jQuery.post(targetUrl, dataFeed, function(data){
	data	= data[0];
	jQuery('input[name=dpd_kode]').prop('placeholder',data.dpd_kode);
	jQuery('input[name=dpd_nama]').prop('placeholder',data.dpd_nama);
	jQuery('input[name=dpd_alamat]').prop('placeholder',data.dpd_alamat);
	jQuery('input[name=dpd_kota]').prop('placeholder',data.dpd_kota);
	jQuery('input[name=dpd_bank]').prop('placeholder',data.dpd_bank);
	jQuery('input[name=dpd_an]').prop('placeholder',data.dpd_an);
	jQuery('input[name=dpd_no_rek]').prop('placeholder',data.dpd_no_rek);	
}, "json");

jQuery('#button-sunting').click(function(){
	jQuery('select').attr('disabled', false);
	jQuery('input').prop('disabled', false);
	jQuery('.sunting').removeClass('hidden');
	jQuery('.rincian').addClass('hidden');
});

jQuery('#button-batal').click(function(){
	jQuery('select').prop('disabled', true);
	jQuery('input').prop('disabled', true);
	jQuery('.sunting').addClass('hidden');
	jQuery('.rincian').removeClass('hidden');
});

jQuery('#button-simpan').click(function(){
	dataFeed 	= {filter: [{name: "dpd_kode", value: kunci.dpdKode }], data: jQuery(':input').serializeArray()};
	jQuery('button').prop('disabled', true);
	targetUrl	= "http://localhost/api/perpamsi/dpp/sunting_dpd.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass(data.kelas);
		jQuery('#pesan div').html(data.pesan);
		jQuery('form').remove();
		jQuery('button').prop('disabled', false);
	}, "json");
});

jQuery('#button-kembali').click(function(){
	loadFile('kembali');
});
