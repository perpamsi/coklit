// inquiry data PDAM
targetUrl   = "/api/perpamsi/rekening/satuan/get_rekap.php";
dataFeed	= dataTemp.proses[dataTemp.procKode];
kunci       = dataFeed.filter[0].value ;
dataProc	= {};

// set nama PDAM / BPAM
jQuery('.block-title h2').html(dataTemp.proses[dataTemp.procKode].pdamNama);
jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataProc['rinci_' + value.pel_no]	= {proses: "rekening_" + dataTemp.applKode, pelNama: value.pel_nama, filter: [{name: "pel_no", value: value.pel_no}]};

		jQuery('#tabel-drd tbody').append('<tr onclick="loadFile(\'rinci_' + value.pel_no + '\')" style=\"cursor: pointer\"><td>' + (i+1) + '</td><td>' + value.pel_nama + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'de'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'de'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'de'}) + '</td></tr>');
	});

	// define parameter tambah user
	dataProc['tambah'] = {proses: "tambah_" + dataTemp.applKode , pdamKode: kunci};
	dataProc.tambah.pdamKode = kunci ;
	// storing kontrol proses
	jQuery.extend(dataTemp.proses, dataProc);
	dataTemp.proses.kembali		= dataTemp.proses[dataTemp.procKode];
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// set tombol kembali ke menu utama
	jQuery('#button-kembali').click(function(){
		loadMenu(dataTemp.applKode);
	});

	// set event tambah
	jQuery('#button-tambah').click(function(){
		loadFile('tambah');
	});

	// rendering tabel
	 jQuery(document).ready(function() {
		 jQuery('#tabel-drd').dataTable( {
			 stateSave: true
		 });
	 });
}, "json");
