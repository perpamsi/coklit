// retrieve data satuan
dataFeed	= dataTemp.proses[dataTemp.procKode];
targetUrl   = "http://localhost/api/perpamsi/dpp/dpd/pdam/get_satuan.php";
jQuery.post(targetUrl, dataFeed, function(data){
	dataFeed	= data.data[0];
	jQuery('input[name=pel_nama]').attr('placeholder',dataFeed.pel_nama);
}, "json");

getSelectUnit();

// set halaman sunting
jQuery('#button-sunting').click(function(){
	jQuery('input').prop('disabled', false);
	jQuery('select').prop('disabled', false);
	jQuery('.sunting').removeClass('hidden');
	jQuery('.rincian').addClass('hidden');
});

// set halaman batal
jQuery('#button-batal').click(function(){
	jQuery('input').prop('disabled', true);
	jQuery('select').prop('disabled', true);
	jQuery('.sunting').addClass('hidden');
	jQuery('.rincian').removeClass('hidden');
});

// set halaman kembali
jQuery('#button-kembali').click(function(){
	// set parameter kembali ke form input rekening
	dataTemp.proses.kembali2	= dataTemp.proses.kembali1;
	dataTemp.proses.kembali1	= dataTemp.proses.kembali;
	dataTemp.proses.kembali		= dataTemp.proses.kembali2;
	delete dataTemp.proses.kembali2;
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	loadFile('kembali');
});
