// set global parameter
dataFeed				= dataTemp.proses[dataTemp.procKode];
kunci					= dataFeed.filter[0].value;
targetUrl				= "/api/perpamsi/get_rekening.php";

jQuery('.block-title h2').html(dataFeed.pelNama);
jQuery.post(targetUrl, {pel_no: kunci}, function(data){
	kunci 	= Math.floor(jQuery('.block').width()/16);
	jQuery('#div-rekening').handsontable({
		data: data.data,
		colHeaders: ["Bulan", "Tahun", "Lembar", "Volume", "Rupiah"],
		colWidths: [kunci, kunci, 4*kunci, 4*kunci, 6*kunci],
		columns: [
			{data: 'bulan',	type: 'numeric'},
			{data: 'tahun',	type: 'numeric'},
			{data: 'lembar', type: 'numeric', format: '0,0', language: 'de'},
			{data: 'volume', type: 'numeric', format: '0,0', language: 'de'},
			{data: 'rupiah', type: 'numeric', format: '0,0', language: 'de'}
		],
		className: "htRight",
		minSpareRows: 1
	});
}, "json");

// set menu kembali
jQuery('#button-kembali').click(function(){
	loadFile('kembali');
});

// set parameter kembali
if(typeof dataTemp.proses.kembali1 == 'object'){
	dataTemp.proses.kembali	= dataTemp.proses.kembali1;
	delete dataTemp.proses.kembali1;
}

// set parameter sunting
dataTemp.proses.sunting	= {proses: 'sunting_' + dataTemp.applKode, filter: dataFeed.filter};
localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

// set menu sunting
jQuery('#button-sunting').click(function(){
	dataTemp.proses.kembali1	= dataTemp.proses[dataTemp.procKode];
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
	loadFile('sunting');
});

// set menu simpan
jQuery('#button-simpan').click(function(){
	jQuery('button').prop('disabled', true);
	dataFeed			= {};
	dataFeed.data		= [];
	jQuery.each(jQuery('#div-rekening').data('handsontable').getData(),function(i,value){
		if (typeof(value.bulan) !== 'undefined' && typeof(value.tahun) !== 'undefined' && typeof(value.lembar) !== 'undefined' && typeof(value.volume) !== 'undefined' && typeof(value.rupiah) !== 'undefined'){
			if (value.bulan !== null && value.tahun !== null && value.lembar !== null && value.volume !== null && value.rupiah !== null){
				dataFeed.data.push({bulan: value.bulan, tahun: value.tahun, lembar: value.lembar, volume: value.volume, rupiah: value.rupiah});
			}
		}
	});
	dataFeed.filter		= dataTemp.proses[dataTemp.procKode].filter;
	if(confirm('Yakin akan menyimpan data perubahan drd ' + dataTemp.proses[dataTemp.procKode].pelNama + ' ?')){
		jQuery('button').prop('disabled', false);
		targetUrl	= "/api/perpamsi/dpp/dpd/pdam/update_drd.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			console.log(data);
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	}
	else{
		jQuery('button').prop('disabled', false);
	}
});
