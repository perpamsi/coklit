// inquiry data PDAM
targetUrl   = "/api/perpamsi/get_kemhan.php";
dataFeed	= {};
pdamKode =  dataTemp.proses.tambah.pdamKode ;
jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery('#option-kemhan').append('<option value="000000"> - </option>');
	jQuery.each(data,function(i,value){
		jQuery('#option-kemhan').append('<option value="'+ value.gp_kode +'">'+ value.gp_nama +'</option>');
	});
}, "json");

function getKotama(param){
	targetUrl  	= '/api/perpamsi/get_kotama.php';
	inHTML		= '<option value="000000" > - </option>';
	dataFeed	= {gp_kode: param};
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery.each(data,function(i,value){		
			inHTML = inHTML + '<option value="'+ value.gp_kode +'" >'+ value.gp_nama +'</option>';
		});
		jQuery('#option-kotama').html(inHTML) ;
	}, "json");
};


jQuery('#option-kemhan').change(function(){
	getKotama(this.value);
});

jQuery('#button-tambah').click(function(){
	jQuery('button').prop('disabled', true);
	dataFeed 	= {data: jQuery(':input').serializeArray()};
	kunci		= dataTemp.proses[dataTemp.procKode].pdamKode;
	dataFeed.data.push({name: 'pdam_kode', value: kunci});
	dataFeed.data.push({name: 'pel_no', value: getToken()});
	targetUrl	= "/api/perpamsi/dpp/dpd/pdam/tambah_satuan.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass(data.kelas);
		jQuery('#pesan div').html(data.pesan);
		jQuery('form').remove();
		jQuery('button').prop('disabled', false);
	}, "json");
});

jQuery('#button-kembali').click(function(){
	loadFile('kembali');
});
