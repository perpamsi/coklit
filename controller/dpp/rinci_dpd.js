targetUrl   = "/api/perpamsi/get_pdam.php";
kunci		= dataTemp.proses[dataTemp.procKode].dpdKode;
dataFeed	= {dpd_kode: kunci};
dataProc	= {};

// set nama PDAM / BPAM
jQuery('.block-title h2').html(dataTemp.proses[dataTemp.procKode].dpdNama);
jQuery.post(targetUrl, dataFeed, function(data) {
	inAttr	= false;
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		dataProc['rinci_' + value.pdam_kode]	= {proses: 'rinci_pdam_' + dataTemp.applKode, pdamNama: value.pdam_nama, pdamKode: value.pdam_kode, filter: [{name: "pdam_kode", value: value.pdam_kode}]};

		jQuery('#tabel-bpam tbody').append('<tr onclick="loadFile(\'rinci_' + value.pdam_kode + '\')" style=\"cursor: pointer\"><td>'+ value.pdam_kode +'</td><td>'+ value.pdam_nama +'</td><td>'+ value.pdam_kota +'</td></tr>');
		inAttr	= true;
	});

	dataProc['tambah'] 	= {proses: "tambah_pdam_" + dataTemp.applKode, dpdKode: kunci};
	dataProc['sunting'] 	= {proses: "sunting_dpd_" + dataTemp.applKode, dpdKode: kunci, dpdNama: dataTemp.proses[dataTemp.procKode].dpdNama, filter: [{name: "pdam_kode", value: kunci}]};
	dataProc['kembali'] = dataTemp.proses[dataTemp.procKode];
	jQuery.extend(dataTemp.proses, dataProc);
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// rendering tabel
	if(inAttr){
		jQuery(document).ready(function(){
			jQuery('#tabel-bpam').dataTable({
				stateSave: true
			});
		});
	}
}, "json");

jQuery('#button-kembali').click(function(){
	loadMenu(dataTemp.applKode);
});

jQuery('#button-tambah').click(function(){
	loadFile('tambah');
});

jQuery('#button-hapus').click(function(){
	dataFeed 	= {filter: [{name: "dpd_kode", value: kunci}]};
	jQuery('button').prop('disabled', true);
	targetUrl	= "/api/perpamsi/dpp/hapus_dpd.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		// kembali ke halaman depan jika eksekusi berhasil
		if(data.errno == 0){
			dataTemp.proses.kembali.proses = dataTemp.applKode;
			localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
		}
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass(data.kelas);
		jQuery('#pesan div').html(data.pesan);
		jQuery('form').remove();
		jQuery('button').prop('disabled', false);
	}, "json");
});

jQuery('#button-sunting').click(function(){
	loadFile('sunting');
});
