jQuery('select').prop('disabled', true);
jQuery('input').prop('disabled', true);

// retrieve informasi PDAM
dataFeed	= dataTemp.proses[dataTemp.procKode];
jQuery('.block-title h2').html(dataFeed.pdamNama);
getSelectDpd();
targetUrl	= "http://localhost/api/perpamsi/dpp/dpd/pdam/get_pdam.php";
jQuery.post(targetUrl, dataFeed, function(data){
	data	= data[0];
	jQuery('input[name=pdam_kode]').prop('placeholder',data.pdam_kode);
	jQuery('input[name=pdam_nama]').prop('placeholder',data.pdam_nama);
	jQuery('input[name=pdam_alamat]').prop('placeholder',data.pdam_alamat);
	jQuery('input[name=pdam_kota]').prop('placeholder',data.pdam_kota);
	jQuery('input[name=pdam_bank]').prop('placeholder',data.pdam_bank);
	jQuery('input[name=pdam_an]').prop('placeholder',data.pdam_an);
	jQuery('input[name=pdam_no_rek]').prop('placeholder',data.pdam_no_rek);	
}, "json");

// pengaturan halaman sunting
jQuery('#button-sunting').click(function(){
	jQuery('select').attr('disabled', false);
	jQuery('input').prop('disabled', false);
	jQuery('.sunting').removeClass('hidden');
	jQuery('.rincian').addClass('hidden');
});

jQuery('#button-batal').click(function(){
	jQuery('select').prop('disabled', true);
	jQuery('input').prop('disabled', true);
	jQuery('.sunting').addClass('hidden');
	jQuery('.rincian').removeClass('hidden');
});

jQuery('#button-simpan').click(function(){
	dataFeed 	= {filter: dataFeed.filter, data: jQuery(':input').serializeArray()};
	jQuery('button').prop('disabled', true);
	targetUrl	= "http://localhost/api/perpamsi/dpp/dpd/sunting_pdam.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass(data.kelas);
		jQuery('#pesan div').html(data.pesan);
		jQuery('form').remove();
		jQuery('button').prop('disabled', false);
	}, "json");
});

jQuery('#button-hapus').click(function(){
	dataFeed 	= {filter: dataFeed.filter};
	jQuery('button').prop('disabled', true);
	targetUrl	= "http://localhost/api/perpamsi/dpp/dpd/hapus_pdam.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass(data.kelas);
		jQuery('#pesan div').html(data.pesan);
		jQuery('form').remove();
		jQuery('button').prop('disabled', false);
	}, "json");
});

jQuery('#button-kembali').click(function(){
	loadFile('kembali');
});
