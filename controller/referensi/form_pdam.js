// halaman data perpamsi : level3 : sunting PDAM
// halaman data perpamsi : level2 : tambah PDAM

// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

// kontrol proses kembali
dataTemp.proses.kembali	= dataTemp.proses.level2;

// storing kontrol proses
localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

if (dataTemp.procKode=='tambahPDAM'){
	jQuery('.sunting').remove();
	jQuery('.rincian').remove();
	jQuery('.tambah').removeClass('hidden');
	getSelectDpd(dataFeed.filter[0].value);
	jQuery('input[name=pdam_kode]').prop('placeholder',dataFeed.filter[0].value);
	jQuery('select').attr('disabled', true);

	jQuery('#button-simpan').click(function(){
		dataFeed 	= {filter: dataFeed.filter, data: jQuery('.formPDAM').serializeArray()};
		jQuery('button').prop('disabled', true);
		targetUrl	= "/api/perpamsi/referensi/tambah_pdam.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	});
}
else {
	jQuery('.rincian').removeClass('hidden');
	jQuery('.tambah').remove();
	jQuery('select').prop('disabled', true);
	jQuery('input').prop('disabled', true);

	// retrieve informasi PDAM
	jQuery('.block-title h2').html(dataFeed.title);
	targetUrl	= "/api/perpamsi/referensi/view_pdam.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		data	= data[0];
		getSelectDpd(data.dpd_kode);
		jQuery('input[name=pdam_kode]').prop('value',data.pdam_kode);
		jQuery('input[name=pdam_nama]').prop('value',data.pdam_nama);
		if(data.pdam_alamat.length>0){
			jQuery('input[name=pdam_alamat]').prop('value',data.pdam_alamat);
		}
		if(data.pdam_kota.length>0){
			jQuery('input[name=pdam_kota]').prop('value',data.pdam_kota);
		}
		if(data.pdam_bank.length>0){
			jQuery('input[name=pdam_bank]').prop('value',data.pdam_bank);
		}
		if(data.pdam_an.length>0){
			jQuery('input[name=pdam_an]').prop('value',data.pdam_an);
		}
		if(data.pdam_no_rek.length>0){
			jQuery('input[name=pdam_no_rek]').prop('value',data.pdam_no_rek);
		}
		jQuery('input[name=pdam_pelanggan]').prop('value',data.pdam_pelanggan);
	}, "json");

	// pengaturan halaman sunting
	jQuery('#button-sunting').click(function(){
		jQuery('select').attr('disabled', false);
		jQuery('input').prop('disabled', false);
		jQuery('.sunting').removeClass('hidden');
		jQuery('.rincian').addClass('hidden');
	});

	jQuery('#button-batal').click(function(){
		jQuery('select').prop('disabled', true);
		jQuery('input').prop('disabled', true);
		jQuery('.sunting').addClass('hidden');
		jQuery('.rincian').removeClass('hidden');
	});

	jQuery('#button-simpan').click(function(){
		dataFeed 	= {filter: dataFeed.filter, data: jQuery('.formPDAM').serializeArray()};
		jQuery('button').prop('disabled', true);
		targetUrl	= "/api/perpamsi/referensi/sunting_pdam.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	});

	jQuery('#button-hapus').click(function(){
		dataFeed 	= {filter: dataFeed.filter};
		jQuery('button').prop('disabled', true);
		targetUrl	= "/api/perpamsi/referensi/hapus_pdam.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	});
}
