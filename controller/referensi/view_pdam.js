// halaman data perpamsi : level2
jQuery('.block-title h2').html(dataFeed.title);

// breadcrumb
if(jQuery('.breadcrumb').hasClass('hide')){
	jQuery('.breadcrumb').removeClass('hide');
}
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// breadcrumb
		inHTML	= '<li onclick="loadMenu(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
				'<li onclick="loadFile(\'kembali\')" style="cursor: pointer">' + dataFeed.title + '</li>';
		// defining kontrol proses
		dataTemp.proses[value.pdam_kode]	= {proses: "rinciPDAM", breadcrumb: inHTML, title: value.pdam_nama, filter: [{name: "pdam_kode", value: value.pdam_kode}]};

		jQuery('#tabel-pdam tbody').append('<tr onclick="loadFile(\'' + value.pdam_kode + '\')" style=\"cursor: pointer\"><td>' + value.pdam_nama + '</td><td>' + value.pdam_kota + '</td></tr>');
	});

	// define parameter kembali
	dataTemp.proses.kembali 	= {proses: dataTemp.applKode};
	dataTemp.proses.level2 		= dataFeed;
	// define parameter sunting dpd
	dataTemp.proses.suntingDPD	= {proses: "suntingDPD", title: dataFeed.title, filter: dataFeed.filter};
	// define parameter tambah pdam
	dataTemp.proses.tambahPDAM	= {proses: "tambahPDAM", title: dataFeed.title, filter: dataFeed.filter};

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// set event kembali
	jQuery('#button-kembali').click(function(){
		loadMenu(dataTemp.applKode);
	});

	// set event sunting
	jQuery('#button-sunting').click(function(){
		loadFile('suntingDPD');
	});

	// set tambah pdam
	jQuery('#button-tambah').click(function(){
		loadFile('tambahPDAM');
	});

	// set event hapus dpd
	jQuery('#button-hapus').click(function(){
		jQuery('button').prop('disabled', true);
		targetUrl	= "/api/perpamsi/referensi/hapus_dpd.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	});

	// rendering tabel
	jQuery(document).ready(function(){
		jQuery('#tabel-pdam').dataTable({
			stateSave: true
		});
	});
}, "json");
