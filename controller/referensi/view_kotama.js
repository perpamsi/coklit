// data referensi kemhan : level2
jQuery('.block-title h2').html(dataFeed.title);
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// breadcrumb
		inHTML	= '<li onclick="loadMenu(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
				'<li onclick="loadFile(\'level2\')" style="cursor: pointer">' + dataFeed.title + '</li>';
		// defining kontrol proses
		dataTemp.proses[value.gp_kode]	= {proses: 'viewSatuan', breadcrumb: inHTML, title: value.gp_nama, filter: [{name: 'gp_kode', value: value.gp_kode}]};

		jQuery('#tabel-kotama tbody').append('<tr onclick="loadFile(\'' + value.gp_kode + '\')" style=\"cursor: pointer\"><td>' + value.gp_roman + '</td><td>' + value.gp_nama + '</td></tr>');
	});

	// define parameter kembali
	dataTemp.proses.level2 	= dataFeed;
	dataTemp.proses.kembali	= {proses: dataTemp.applKode}

	// breadcrumb
	if(jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').removeClass('hide');
	}
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery(document).ready(function(){
		jQuery('#tabel-kotama').dataTable({
			stateSave: true
		});
	});
}, "json");
