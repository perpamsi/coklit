// halaman data perpamsi : front
jQuery.post(targetUrl, {}, function(data){
	jQuery.each(data,function(i,value){
		// breadcrumb
		inHTML	= '<li onclick="loadMenu(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>';
		// defining kontrol proses
		dataTemp.proses[value.dpd_kode]	= {proses: 'rinciDPD', breadcrumb: inHTML, title: value.dpd_nama, filter: [{name: 'dpd_kode', value: value.dpd_kode}]};

		jQuery('#tabel-dpd tbody').append('<tr onclick="loadFile(\'' + value.dpd_kode + '\')" style="cursor: pointer"><td>' + value.dpd_nama+ '</td></tr>');
	});

	// storing kontrol proses
	dataTemp.proses.kembali		= {proses: dataTemp.applKode};
	dataTemp.proses.tambahDPD	= {proses: 'tambahDPD', title: 'Form Tambah DPD'};
	localStorage.setItem('perpamsi', JSON.stringify(dataTemp));

	jQuery('#button-tambah').click(function(){
		loadFile('tambahDPD');
	});

	jQuery(document).ready(function(){
		jQuery('#tabel-dpd').dataTable({
			stateSave: true
		});
	});

}, "json");
