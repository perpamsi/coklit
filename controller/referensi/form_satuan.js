// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

// data referensi kemhan : level4
jQuery('.block-title h2').html(dataFeed.title);
jQuery('.simpan').remove();
jQuery('input').prop('disabled', true);
jQuery('select').prop('disabled', true);

jQuery.post(targetUrl, dataFeed, function(data){
	jQuery('input[name=pel_nama]').attr('value',data[0].pel_nama);
	getUnitOrganisasi(data[0].gp_parent);
	getKotama(data[0].gp_kode, data[0].gp_parent);
	getSelectDpd(data[0].dpd_kode);
	getSelectPdam(data[0].dpd_kode, data[0].pdam_kode);
}, "json");

// perbaharuan data kotama
jQuery('#option-kemhan').change(function(){
	// jika unit organisasi KEMHAN dan MABES TNI
	if(this.value=='010101' || this.value=='010201'){
		kunci = this.value;
	}
	else{
		kunci = '000000';
	}
	getKotama(kunci, this.value);
});

// perbaharuan data pdam
jQuery('#option-dpd').change(function(){
	getSelectPdam(this.value, this.value);
});

// define proses kembali
dataTemp.proses.kembali	= dataTemp.proses.level3;
// storing kontrol proses
localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

// set halaman sunting
jQuery('#button-sunting').click(function(){
	jQuery('input').prop('disabled', false);
	jQuery('select').prop('disabled', false);
	jQuery('.sunting').removeClass('hidden');
	jQuery('.rincian').addClass('hidden');
});

// set halaman batal
jQuery('#button-batal').click(function(){
	jQuery('input').prop('disabled', true);
	jQuery('select').prop('disabled', true);
	jQuery('.sunting').addClass('hidden');
	jQuery('.rincian').removeClass('hidden');
});

// proses sunting satuan
jQuery('#button-simpan').click(function(){
	jQuery('button').prop('disabled', true);
	targetUrl	= "/api/perpamsi/referensi/sunting_satuan.php";
	jQuery.post(targetUrl, {filter: dataFeed.filter, data: jQuery('.formSatuan').serializeArray()}, function(data){
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass(data.kelas);
		jQuery('#pesan div').html(data.pesan);
		jQuery('form').remove();
		jQuery('button').prop('disabled', false);
	}, "json");
});

// proses hapus satuan
jQuery('#button-delete').click(function(){
	if(confirm('Yakin akan menghapus ' + dataFeed.title + ' ?')){
		jQuery('button').prop('disabled', true);
		targetUrl	= "/api/perpamsi/referensi/hapus_satuan.php";
		jQuery.post(targetUrl, {filter: dataFeed.filter}, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	}
	else{
		jQuery('button').prop('disabled', false);
	}
});
