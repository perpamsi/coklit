// data referensi kemhan : level3
jQuery('.block-title h2').html(dataFeed.title);
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// breadcrumb
		inHTML	= dataFeed.breadcrumb +
				'<li onclick="loadFile(\'kembali\')" style="cursor: pointer">' + dataFeed.title + '</li>';
		// define rincian satuan
		dataTemp.proses[value.pel_no]	= {proses: 'formSatuan', breadcrumb: inHTML, title: value.pel_nama, filter: [{name: 'pel_no', value: value.pel_no}]};

		jQuery('#tabel-satuan tbody').append('<tr onclick="loadFile(\'' + value.pel_no + '\')" style="cursor: pointer"><td>' + value.pel_nama + '</td><td>' + value.pdam_nama + '</td></tr>');
	});

	// define parameter kembali
	dataTemp.proses.level3	= dataFeed;
	// define parameter kembali dari kotama
	if(typeof(dataTemp.proses.level2)=='object'){
		dataTemp.proses.kembali	= dataTemp.proses.level2;
	}
	else{
		dataTemp.proses.kembali	= {proses: dataTemp.applKode}
	}

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// breadcrumb
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// rendering tabel
	jQuery(document).ready(function(){
		jQuery('#tabel-satuan').dataTable({
			stateSave: true
		});
	});
}, "json");
