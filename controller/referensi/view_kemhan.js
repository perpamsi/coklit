// data referensi kemhan : front
jQuery.post(targetUrl, {}, function(data){
	jQuery.each(data,function(i,value){
		// breadcrumb
		inHTML	= '<li onclick="loadMenu(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>';
		// defining kontrol proses
		if(value.gp_kode=='010101' || value.gp_kode=='010201'){
			dataTemp.proses[value.gp_kode]	= {proses: 'viewSatuan', breadcrumb: inHTML, title: value.gp_nama, filter: [{name: 'gp_kode', value: value.gp_kode}]};
		}
		else{
			dataTemp.proses[value.gp_kode]	= {proses: 'viewKotama', breadcrumb: inHTML, title: value.gp_nama, filter: [{name: 'gp_parent', value: value.gp_kode}]};
		}

		jQuery('#tabel-kemhan tbody').append('<tr onclick="loadFile(\'' + value.gp_kode + '\')" style="cursor: pointer"><td>' + value.gp_nama + '</td></tr>');
	});

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
	console.log(dataTemp);

	// rendering table
	jQuery('#tabel-kemhan').dataTable({
		stateSave: true
	});
}, "json");
