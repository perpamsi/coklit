// halaman data perpamsi : front  : tambah DPD
// halaman data perpamsi : level2 : sunting DPD
jQuery('.block-title h2').html(dataFeed.title);
if(dataTemp.procKode=='suntingDPD'){
	jQuery.post(targetUrl, dataFeed, function(data){
		data	= data[0];
		jQuery('input[name=dpd_kode]').prop('value',data.dpd_kode);
		jQuery('input[name=dpd_nama]').prop('value',data.dpd_nama);
		if(data.dpd_alamat.length>0){
			jQuery('input[name=dpd_alamat]').prop('placeholder',data.dpd_alamat);
		}
		if(data.dpd_bank.length>0){
			jQuery('input[name=dpd_bank]').prop('placeholder',data.dpd_bank);
		}
		if(data.dpd_an.length>0){
			jQuery('input[name=dpd_an]').prop('placeholder',data.dpd_an);
		}
		if(data.dpd_no_rek.length>0){
			jQuery('input[name=dpd_no_rek]').prop('placeholder',data.dpd_no_rek);
		}
	}, "json");

	jQuery('#button-simpan').click(function(){
		dataFeed 			= {filter: dataFeed.filter, data: jQuery('.formDPD').serializeArray()};
		if(dataFeed.data[0].value.length>0 && dataFeed.data[1].value.length>0){
			jQuery('button').prop('disabled', true);
			targetUrl	= "/api/perpamsi/referensi/sunting_dpd.php";
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div').addClass(data.kelas);
				jQuery('#pesan div').html(data.pesan);
				jQuery('form').remove();
				jQuery('button').prop('disabled', false);
			}, "json");
		}
	});

	jQuery('#button-kembali').click(function(){
		loadFile('level2');
	});
}
else{	
	jQuery('#button-simpan').click(function(){
		dataFeed 			= {data: jQuery(':input').serializeArray()};
		if(dataFeed.data[0].value.length>0 && dataFeed.data[1].value.length>0){
			jQuery('button').prop('disabled', true);
			targetUrl	= "/api/perpamsi/referensi/tambah_dpd.php";
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div').addClass(data.kelas);
				jQuery('#pesan div').html(data.pesan);
				jQuery('form').remove();
				jQuery('button').prop('disabled', false);
			}, "json");
		}
	});

	jQuery('#button-kembali').click(function(){
		loadMenu(dataTemp.applKode);
	});
}
