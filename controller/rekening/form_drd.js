// halaman input rekening : level3
jQuery('.block-title').remove();

// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

jQuery.post(targetUrl, dataFeed, function(data){
	if(data[0].approved==1 && jQuery('#button-setuju').hasClass('hidden')){
		jQuery('#button-setuju').removeClass('hidden');
	}
	kunci 	= Math.floor(jQuery('.block').width()/9);
	jQuery('#div-rekening').handsontable({
		data: data,
		colHeaders: ["Bulan", "Tahun", "Sambungan", "Volume", "Rupiah"],
		colWidths: [kunci, kunci, kunci, 2*kunci, 4*kunci],
		columns: [
			{data: 'bulan',	type: 'numeric'},
			{data: 'tahun',	type: 'numeric'},
			{data: 'lembar', type: 'numeric', format: '0,0', language: 'de'},
			{data: 'volume', type: 'numeric', format: '0,0', language: 'de'},
			{data: 'rupiah', type: 'numeric', format: '0,0', language: 'de'}
		],
		className: "htRight",
		minSpareRows: 1
	});
}, "json");

// set menu kembali
jQuery('#button-kembali').click(function(){
	loadFile('kembali');
});

// set parameter kembali
if(typeof dataTemp.proses.level2 == 'object'){
	dataTemp.proses.kembali	= dataTemp.proses.level2;
	delete dataTemp.proses.level1;
}

// set menu simpan
jQuery('#button-simpan').click(function(){
	jQuery('button').prop('disabled', true);
	delete dataFeed.proses;
	dataFeed.data		= [];
	jQuery.each(jQuery('#div-rekening').data('handsontable').getData(),function(i,value){
		if (typeof(value.bulan) !== 'undefined' && typeof(value.tahun) !== 'undefined' && typeof(value.lembar) !== 'undefined' && typeof(value.volume) !== 'undefined' && typeof(value.rupiah) !== 'undefined'){
			if (value.bulan !== null && value.tahun !== null && value.lembar !== null && value.volume !== null && value.rupiah !== null){
				dataFeed.data.push({bulan: value.bulan, tahun: value.tahun, lembar: value.lembar, volume: value.volume, rupiah: value.rupiah});
			}
		}
	});
	if(confirm('Yakin akan menyimpan data perubahan tagihan ' + dataFeed.title + ' ?')){
		jQuery('button').prop('disabled', false);
		targetUrl	= "/api/perpamsi/rekening/kemhan/kotama/satuan/push_input.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	}
	else{
		jQuery('button').prop('disabled', false);
	}
});

// set menu setuju
jQuery('#button-setuju').click(function(){
	jQuery('button').prop('disabled', true);
	delete dataFeed.proses;
	if(confirm('Yakin akan melakukan validasi tagihan ' + dataFeed.title + ' ?')){
		jQuery('button').prop('disabled', false);
		targetUrl	= "/api/perpamsi/rekening/kemhan/kotama/satuan/push_update.php";
		jQuery.post(targetUrl, dataFeed, function(data){
			jQuery('#pesan').removeClass('hidden');
			jQuery('#pesan div').addClass(data.kelas);
			jQuery('#pesan div').html(data.pesan);
			jQuery('form').remove();
			jQuery('button').prop('disabled', false);
		}, "json");
	}
	else{
		jQuery('button').prop('disabled', false);
	}
});
