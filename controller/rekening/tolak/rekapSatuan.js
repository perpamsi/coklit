// halaman tolak rekening : front : tambah : pilih unit organisasi : rekap satuan
jQuery.post(targetUrl, dataFeed, function(data) {
	dataTemp.proses[dataTemp.applKode]	= {proses: dataTemp.applKode};
	jQuery.each(data,function(i,value){
		// define control proses
		inHTML	= 	'<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'tambah\')" style="cursor: pointer">Pilih Unit Organisasi</li>' +
					'<li onclick="loadFile(\'' + value.gp_parent + '\')" style="cursor: pointer">' + value.unit_organisasi + '</li>' +
					'<li>' + value.pel_nama + '</li>';
		dataTemp.proses[value.pel_no]	= {proses: 'rinciSatuan', title: value.bln_rekening, breadcrumb: inHTML, filter: [{name: 'pel_no', value: value.pel_no}, {name: 'rek_bln', value: value.rek_bln}, {name: 'rek_thn', value: value.rek_thn}]};

		inHTML	=	'<tr onclick="loadFile(\'' + value.pel_no + '\')" style="cursor: pointer">' +
						'<td align="right">' + (i+1)	+ '</td>' +
						'<td>' + value.pel_nama 		+ '</td>' +
						'<td class="hidden-xs">' + value.gp_nama 		+ '</td>' +
						'<td class="hidden-xs">' + value.pdam_nama 		+ '</td>' +
						'<td>' + value.bln_rekening 	+ '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td>' +
					'</tr>';
		jQuery('#tabel-rekap-satuan tbody').append(inHTML);
	});

	// define proses kembali
	dataTemp.proses.kembali		= dataTemp.proses.tambah;
	dataTemp.proses.rekapSatuan	= dataFeed;

	// storing kontrol proses
	localStorage.setItem('perpamsi', JSON.stringify(dataTemp));

	// rendering tabel
	jQuery('#tabel-rekap-satuan').dataTable({
		stateSave: true
	});

	// breadcrumb
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// button - event - kembali
	inHTML	= 	'<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')">' +
					'<i class="fa fa-arrow-circle-left"></i> Kembali' +
				'</button>' +
				'<div class="clear-fix"></div>';
	jQuery('.block').append(inHTML);
}, 'json');
