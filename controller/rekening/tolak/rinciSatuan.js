// halaman tolak rekening : front : tambah : pilih unit organisasi : rekap satuan : rinci satuan
jQuery.post(targetUrl, dataFeed, function(data){
	dataFeed.temp	= [];
	dataFeed.filter	= [];
	jQuery.each(data,function(i,value){
		dataFeed.temp.push({name: 'pel_no', value: value.pel_no}, {name: 'pdam_kode', value: value.pdam_kode});
		dataFeed.filter.push({name: 'rek_nomor', value: value.rek_nomor});
		inHTML	=	'<tr>' +
						'<td>' + value.pel_nosl 	+ '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}) 	+ '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) 	+ '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) 		+ '</td>' +
					'</tr>';
		jQuery('#tabel-rinci-satuan tbody').append(inHTML);
	});

	// define proses input tolak
	dataTemp.proses.formTunda	= {proses: 'formTolak', filter: dataFeed.temp};
	delete dataFeed.temp;

	// define proses kembali
	dataTemp.proses.kembali		= dataTemp.proses.rekapSatuan;
	dataTemp.proses.rinciSatuan	= dataFeed;

	// storing kontrol proses
	localStorage.setItem('perpamsi', JSON.stringify(dataTemp));

	// rendering tabel
	jQuery('#tabel-rinci-satuan').dataTable({
		stateSave: true
	});

	// breadcrumb
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// title
	if(jQuery('.block-title').hasClass('hide')){
		jQuery('.block-title').removeClass('hide');
	}
	jQuery('.block-title h2').html(dataFeed.title);

	// button - event - kembali
	inHTML	= 	'<div class="btn-group">' +
					'<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')">' +
						'<i class="fa fa-arrow-circle-left"></i> Kembali' +
					'</button>' +
					'<button class="btn btn-sm btn-info" onclick="loadFile(\'formTunda\')">' +
						'<i class="gi gi-circle_plus"></i> Input Tagihan' +
					'</button>' +
					'<button id="button-tolak" class="btn btn-sm btn-warning">' +
						'<i class="fa fa-paperclip"></i> Tolak Tagihan' +
					'</button>' +
				'</div>' +
				'<div class="clear-fix"></div>';
	jQuery('.block').append(inHTML);

	// event tolak
	jQuery('#button-tolak').click(function(){
		jQuery('button').prop('disabled', true);
		if(confirm('Yakin akan menyimpan perubahan data?')){
			targetUrl	= "/api/perpamsi/kemhan/tolak/push_update.php";
			delete dataFeed.proses;
			delete dataFeed.title;
			delete dataFeed.breadcrumb;
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div').addClass(data.kelas);
				jQuery('#pesan div').html(data.pesan);
				jQuery('.table-responsive').remove();
				jQuery('.btn-group').remove();
				jQuery('button').prop('disabled', false);
			}, "json");
		}
		else{
			jQuery('button').prop('disabled', false);
		}
	});
}, 'json');
