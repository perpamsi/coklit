// halaman tolak rekening : front
jQuery.post(targetUrl, {}, function(data){
	dataTemp.proses[dataTemp.applKode]	= {proses: dataTemp.applKode};
	jQuery.each(data,function(i,value){
		// define kontrol proses
		inHTML	=	'<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li>Rekening ' + value.bln_rekening + '</li>';
		dataTemp.proses[value.pel_no]	= {proses: 'rinciTolak', title: value.pel_nama, breadcrumb: inHTML, filter: [{name: 'pel_no', value: value.pel_no}]};

		inHTML	=	'<tr onclick="loadFile(\'' + value.pel_no + '\')" style="cursor: pointer">' +
						'<td align="right">' + (i+1)	+ '</td>' +
						'<td>' + value.pel_nama 		+ '</td>' +
						'<td class="hidden-xs">' + value.gp_nama 		+ '</td>' +
						'<td class="hidden-xs">' + value.pdam_nama 		+ '</td>' +
						'<td>' + value.bln_rekening 	+ '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td>' +
					'</tr>';
		jQuery('#tabel-rekap-satuan tbody').append(inHTML);
	});

	// define kontrol proses : tambah rekening tunda
	if(typeof(dataFeed.breadcrumb)=='string'){
		inHTML	= 	dataFeed.breadcrumb;
	}
	else{
		inHTML	= 	'<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'tambah\')" style="cursor: pointer">Pilih Unit Organisasi</li>';
	}
	dataTemp.proses.tambah	= {proses: 'viewKemhan', breadcrumb: inHTML};
	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery('#tabel-rekap-satuan').dataTable( {
		stateSave: true
	} );

	// button - event
	inHTML	= 	'<button class="btn btn-sm btn-success" onclick="loadFile(\'tambah\')">' +
					'<i class="gi gi-circle_plus"></i> Tambah' +
				'</button>' +
				'<div class="clear-fix"></div>';
	jQuery('.block').append(inHTML);
}, "json");
