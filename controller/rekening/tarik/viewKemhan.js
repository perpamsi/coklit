// halaman tarik rekening : front : tambah : pilih unit organisasi
jQuery.post(targetUrl, {}, function(data){
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		inHTML	= 	'<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'tambah\')" style="cursor: pointer">Pilih Unit Organisasi</li>' +
					'<li onclick="loadFile(\'' + value.gp_kode + '\')" style="cursor: pointer">' + value.gp_nama + '</li>';
		dataTemp.proses[value.gp_kode]	= {proses: 'rekapSatuan', breadcrumb: inHTML, filter: [{name: 'gp_parent', value: value.gp_kode}]};

		inHTML	= 	'<tr onclick="loadFile(\'' + value.gp_kode + '\')" style="cursor: pointer">' +
						'<td>' + value.gp_nama + '</td>' +
					'</tr>';
		jQuery('#tabel-kemhan tbody').append(inHTML);
	});

	// define kontrol proses : kembali
	dataTemp.proses.kembali	= {proses: dataTemp.applKode};
	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery('#tabel-kemhan').dataTable( {
		stateSave: true
	} );

	// breadcrumb
	if(jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').removeClass('hide');
	}
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// button - event - kembali
	inHTML	= 	'<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')">' +
					'<i class="fa fa-arrow-circle-left"></i> Kembali' +
				'</button>' +
				'<div class="clear-fix"></div>';
	jQuery('.block').append(inHTML);
}, "json");
