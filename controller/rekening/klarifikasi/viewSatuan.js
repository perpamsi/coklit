// halaman klarifikasi rekening : front : tambah : pilih unit organisasi : view satuan
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// define kontrol proses
		dataTemp.proses[value.pel_no]	= {proses: 'formKlarifikasi', title: value.pel_nama, filter: [{name: 'pel_no', value: value.pel_no}, {name: 'pdam_kode', value: value.pdam_kode}]};
		inHTML	=	'<tr onclick="loadFile(\'' + value.pel_no + '\')" style="cursor: pointer">' +
						'<td align="right">' + (i+1) 	+ '</td>' +
						'<td>' + value.pel_nama 		+ '</td>' +
						'<td>' + value.pdam_nama 		+ '</td>' +
					'</tr>';
		jQuery('#tabel-satuan tbody').append(inHTML);
	});

	// define proses kembali
	dataTemp.proses.kembali		= {proses: 'viewKemhan'};
	dataTemp.proses.viewSatuan	= dataFeed;

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery('#tabel-satuan').dataTable({
		stateSave: true
	});

	// breadcrumb
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// button - event - kembali
	inHTML	= 	'<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')">' +
					'<i class="fa fa-arrow-circle-left"></i> Kembali' +
				'</button>' +
				'<div class="clear-fix"></div>';
	jQuery('.block').append(inHTML);
}, "json");
