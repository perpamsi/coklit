// halaman klarifikasi rekening : front : tambah : pilih unit organisasi : view satuan : form input
jQuery.post(targetUrl, dataFeed, function(data){
	kunci 	= Math.floor(jQuery('.block').width()/10);
	jQuery('#div-rekening').handsontable({
		data: data,
		colHeaders: ["Nomer SL", "Bulan", "Tahun", "Volume", "Rupiah"],
		colWidths: [kunci, kunci, kunci, 3*kunci, 4*kunci],
		columns: [
			{data: 'pel_nosl'},
			{data: 'rek_bln',		type: 'numeric'},
			{data: 'rek_thn',		type: 'numeric'},
			{data: 'rek_st_pakai', 	type: 'numeric', format: '0,0', language: 'en'},
			{data: 'rek_total', 	type: 'numeric', format: '0,0', language: 'en'}
		],
		className: "htRight",
		minSpareRows: 1
	});
}, "json");

// define kontrol proses kembali
dataTemp.proses.kembali	= dataTemp.proses.viewSatuan;

// storing kontrol proses
localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

// button - event
jQuery(document).ready(function(){
	jQuery('#button-simpan').click(function(){
		jQuery('button').prop('disabled', true);
		dataFeed.data	= [];
		jQuery.each(jQuery('#div-rekening').data('handsontable').getData(),function(i,value){
			if(value.pel_nosl!=null && value.rek_bln!=null && value.rek_thn!=null && value.rek_st_pakai!=null && value.rek_total!=null){
				dataFeed.data.push({pel_nosl: value.pel_nosl, rek_bln: value.rek_bln, rek_thn: value.rek_thn, rek_st_pakai: value.rek_st_pakai, rek_total: value.rek_total});
			}
		});

		if(confirm('Yakin akan menyimpan perubahan data?')){
			delete dataFeed.proses;
			delete dataFeed.breadcrumb;
			delete dataFeed.title;
			targetUrl	= "/api/perpamsi/rekening/klarifikasi/push_insert.php";
			jQuery.post(targetUrl, dataFeed, function(data){
				jQuery('#pesan').removeClass('hidden');
				jQuery('#pesan div').addClass(data.kelas);
				jQuery('#pesan div').html(data.pesan);
				jQuery('form').remove();
				jQuery('button').prop('disabled', false);
			}, "json");
		}
		jQuery('button').prop('disabled', false);
	});
});	
