// halaman input rekening : level2 : tambah satuan
targetUrl   = "/api/perpamsi/referensi/view_kemhan.php";
jQuery.post(targetUrl, {}, function(data) {
	jQuery('#option-kemhan').append('<option value="000000"> - </option>');
	jQuery.each(data,function(i,value){
		jQuery('#option-kemhan').append('<option value="'+ value.gp_kode +'">'+ value.gp_nama +'</option>');
	});
}, "json");

function getKotama(param){
	targetUrl  	= '/api/perpamsi/referensi/view_kotama.php';
	inHTML		= '<option value="000000" > - </option>';
	dataFeed	= {filter: [{name: "gp_parent", value: param}]};
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery.each(data,function(i,value){		
			inHTML = inHTML + '<option value="'+ value.gp_kode +'" >'+ value.gp_nama +'</option>';
		});
		jQuery('#option-kotama').html(inHTML) ;
	}, "json");
};


jQuery('#option-kemhan').change(function(){
	getKotama(this.value);
});

jQuery('#button-tambah').click(function(){
	jQuery('button').prop('disabled', true);
	dataFeed 	= {data: jQuery(':input').serializeArray()};
	dataFeed.data.push(dataTemp.proses[dataTemp.procKode].data[0]);
	targetUrl	= "/api/perpamsi/referensi/tambah_satuan.php";
	jQuery.post(targetUrl, dataFeed, function(data){
		jQuery('#pesan').removeClass('hidden');
		jQuery('#pesan div').addClass(data.kelas);
		jQuery('#pesan div').html(data.pesan);
		jQuery('form').remove();
		jQuery('button').prop('disabled', false);
	}, "json");
});

jQuery('#button-kembali').click(function(){
	loadFile('kembali');
});
