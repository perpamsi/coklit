// halaman input rekening : front
jQuery.post(targetUrl, {}, function(data) {
	jQuery.each(data,function(i,value){
		// breadcrumb
		inHTML	= '<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
				'<li onclick="loadFile(\'' + value.pdam_kode + '\')" style="cursor: pointer">' + value.pdam_nama + '</li>';
		// defining kontrol proses
		dataTemp.proses[value.pdam_kode] = {proses: "rinciDRD", title: value.pdam_nama, breadcrumb: inHTML, filter: [{name: "pdam_kode", value: value.pdam_kode}]};
		
		jQuery('#tabel-drd-rekap tbody').append('<tr onclick="loadFile(\'' + value.pdam_kode + '\')" style=\"cursor: pointer\"><td>' + value.dpd_nama + '</td><td>' + value.pdam_kota + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td></tr>');
	});

	// menu param
	dataTemp.proses[dataTemp.applKode]	= {};
	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
	
	// rendering tabel
	jQuery(document).ready(function() {
		jQuery('#tabel-drd-rekap').dataTable( {
			stateSave: true
		} );
	} );
}, "json");
