// breadcrumb
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

// append button kembali
inHTML	= '<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')"><i class="fa fa-arrow-circle-left"></i> Kembali</button>' +
	'<div class="clear-fix"></div>';
jQuery('.block').append(inHTML);

jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// kontrol proses kembali
		kunci	= value.pel_no;
		// breadcrumb
		inHTML	= dataFeed.breadcrumb +
				'<li onclick="loadFile(\'' + value.rek_nomor + '\')" style="cursor: pointer">' + value.bln_rekening + '</li>';
		// kontrol proses
		dataTemp.proses[value.rek_nomor]	= {proses: "formApprove", breadcrumb: inHTML, title: 'Form Approval', filter: [{name: 'rek_nomor', value: value.rek_nomor}]};
		inHTML	= '<tr onclick="loadFile(\'' + value.rek_nomor + '\')" style="cursor: pointer">' +
				'<td>' + (i + 1) + '</td>' +
				'<td>' + value.bln_rekening + '</td>' +
				'<td>' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}) + '</td>' +
				'<td>' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td>' +
				'<td>' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td>' +
				'<td>' + value.status + '</td>' +
			'</tr>';
		jQuery('#tabel-rekap-rekening tbody').append(inHTML);
	});

	// storing kontrol proses
	dataTemp.proses.kembali	= dataTemp.proses[kunci];
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
}, 'json');
