// halaman input rekening : level2
jQuery('.block-title').remove();

// breadcrumb
if(jQuery('.breadcrumb').hasClass('hide')){
	jQuery('.breadcrumb').removeClass('hide');
}
jQuery('.breadcrumb').html(dataFeed.breadcrumb);

jQuery.post(targetUrl, dataFeed, function(data) {
	jQuery.each(data,function(i,value){
        if(typeof value.pel_nama == 'string'){
			// breadcrumb
			inHTML	= '<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'' + value.pdam_kode + '\')" style="cursor: pointer">' + dataFeed.title + '</li>' +
					'<li onclick="loadFile(\'' + value.pel_no + '\')" style="cursor: pointer">' + value.pel_nama + '</li>';
			// defining kontrol proses
			if(dataTemp.applKode=='020101'){
				dataTemp.proses[value.pel_no]	= {proses: "tambahDRD", title: value.pel_nama, breadcrumb: inHTML, filter: [{name: "pel_no", value: value.pel_no}, {name: "pdam_kode", value: value.pdam_kode}]};
			}
			else{
				dataTemp.proses[value.pel_no]	= {proses: "approveDRD", title: value.pel_nama, breadcrumb: inHTML, filter: [{name: "pel_no", value: value.pel_no}, {name: "pdam_kode", value: value.pdam_kode}]};
			}

			jQuery('#tabel-drd-rinci tbody').append('<tr onclick="loadFile(\'' + value.pel_no + '\')" style=\"cursor: pointer\"><td>' + value.pel_nama + '</td><td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'en'}) + '</td><td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'en'}) + '</td><td>' + value.status + '</td></tr>');
        }
	});

	// define parameter navigasi
	dataTemp.proses.tambah 	= {proses: "tambahSatuan", data: dataFeed.filter};
	dataTemp.proses.kembali	= {};
	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// set tombol kembali ke menu utama
	jQuery('#button-kembali').click(function(){
		loadFile('kembali');
	});

	// set event tambah
	jQuery('#button-tambah').click(function(){
		loadFile('tambah');
	});

	// rendering tabel
	 jQuery(document).ready(function() {
		 jQuery('#tabel-drd-rinci').dataTable( {
			 stateSave: true
		 });
	 });
}, "json");
