jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// arsip tagihan
		if(dataTemp.applKode=='060101'){
			inHTML	=	'<tr>' +
							'<td>' + (i+1) + '</td>' +
							'<td>' + value.pel_nama + '</td>' +
							'<td>' + value.bln_rekening + '</td>' +
							'<td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'us'}) + '</td>' +
							'<td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}) + '</td>' +
							'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}) + '</td>' +
							'<td>' + value.rek_ket + '</td>' +
						'</tr>';
		}
		// arsip rekening
		else{
			inHTML	=	'<tr>' +
							'<td>' + (i+1) + '</td>' +
							'<td>' + value.pel_nama + '</td>' +
							'<td>' + value.bln_tagihan + '</td>' +
							'<td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'us'}) + '</td>' +
							'<td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}) + '</td>' +
							'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}) + '</td>' +
							'<td>' + value.rek_ket + '</td>' +
						'</tr>';
		}
		jQuery('#tabel-rinci-rekening tbody').append(inHTML);
	});

	// define proses kembali
	dataTemp.proses.kembali	= dataTemp.proses.level2;

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// breadcrumb
	if(jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').removeClass('hide');
	}
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// button - event
	inHTML	=	'<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')">' +
					'<i class="fa fa-arrow-circle-left"></i> Kembali' +
				'</button>' +
				'<div class="clear-fix"></div>';
	jQuery('.block').append(inHTML);

	// rendering tabel
	jQuery('#tabel-rinci-rekening').dataTable({
		stateSave: true
	});
}, "json");
