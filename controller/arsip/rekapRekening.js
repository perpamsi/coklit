jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// define kontrol proses
		dataTemp.proses[dataTemp.applKode]	= {proses: dataTemp.applKode};
		inHTML	=	'<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'' + value.pdam_kode + '\')" style="cursor: pointer">' + dataFeed.title + '</li>' +
					'<li>Rekening ' + value.bln_rekening + '</li>';
		dataTemp.proses[value.bln_rekening]	= {proses: 'rinciRekening', breadcrumb: inHTML, filter: [{name: 'pdam_kode', value: value.pdam_kode}, {name: 'rek_bln', value: value.rek_bln}, {name: 'rek_thn', value: value.rek_thn}]};

		inHTML	=	'<tr onclick="loadFile(\'' + value.bln_rekening + '\')" style=\"cursor: pointer\">' +
						'<td>' + (i+1) + '</td>' +
						'<td>' + value.bln_rekening + '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_lembar, {format:'#,###', locale:'us'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_st_pakai, {format:'#,###', locale:'us'}) + '</td>' +
						'<td align="right">' + jQuery.formatNumber(value.rek_total, {format:'#,###', locale:'us'}) + '</td>' +
						'<td>' + value.status + '</td>' +
					'</tr>';
		jQuery('#tabel-rekap-rekening tbody').append(inHTML);
	});

	// define proses kembali
	dataTemp.proses.kembali	= {proses: dataTemp.applKode};
	dataTemp.proses.level2	= dataFeed;

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// breadcrumb
	if(jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').removeClass('hide');
	}
	jQuery('.breadcrumb').html(dataFeed.breadcrumb);

	// button - event
	inHTML	=	'<button class="btn btn-sm btn-info" onclick="loadFile(\'kembali\')">' +
					'<i class="fa fa-arrow-circle-left"></i> Kembali' +
				'</button>' +
				'<div class="clear-fix"></div>';
	jQuery('.block').append(inHTML);

	// rendering tabel
	jQuery('#tabel-rekap-rekening').dataTable({
		stateSave: true
	});
}, "json");
