// arsip pengajuan : front
// arsip rekening : front
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data,function(i,value){
		// defining kontrol proses
		inHTML	=	'<li onclick="loadFile(\'' + dataTemp.applKode + '\')" style="cursor: pointer">' + applName + '</li>' +
					'<li onclick="loadFile(\'' + value.pdam_kode + '\')" style="cursor: pointer">' + value.pdam_nama + '</li>';
		// arsip tagihan
		if(kunci=='060101'){
			dataTemp.proses[value.pdam_kode]	= {proses: 'rekapTagihan', title: value.pdam_nama, breadcrumb: inHTML, filter: [{name: 'pdam_kode', value: value.pdam_kode}]};
		}
		// arsip rekening
		else{
			dataTemp.proses[value.pdam_kode]	= {proses: 'rekapRekening', title: value.pdam_nama, breadcrumb: inHTML, filter: [{name: 'pdam_kode', value: value.pdam_kode}]};
		}

		jQuery('#tabel-pdam tbody').append('<tr onclick="loadFile(\'' + value.pdam_kode + '\')" style="cursor: pointer"><td>' + value.pdam_nama + '</td><td>' + value.pdam_kota + '</td></tr>');
	});

	// storing kontrol proses
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	// rendering tabel
	jQuery(document).ready(function(){
		jQuery('#tabel-pdam').dataTable({
			stateSave: true
		});
	});
}, "json");
