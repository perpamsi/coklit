jQuery('#main-container').css('min-height', jQuery(window).height());
jQuery('#page-content').css('min-height', jQuery(window).height() - 84);

jQuery.ajaxSetup ({
    cache: false
});

function toggleMenu(param){
	jQuery('.000000 li ul').fadeIn().css('display', 'none');
	jQuery('.' + param).css('display', 'block');
}

function getToken(){
    randId = new Date();
    return randId.getTime();
}

// fungsi form select unit organisasi
function getSelectUnit(){
	targetUrl   = "/api/perpamsi/referensi/view_kemhan.php";
	jQuery.post(targetUrl, {}, function(data) {
		jQuery('#option-kemhan').append('<option value="-"> - </option>');
		jQuery.each(data,function(i,value){		
			jQuery('#option-kemhan').append('<option value="'+ value.gp_kode +'">'+ value.gp_nama +'</option>');
		});
	}, "json");
}

// fungsi form select grup pengguna
function getSelectGrup(grupId){
	targetUrl   = "/api/perpamsi/referensi/view_grup.php";
	jQuery.post(targetUrl, {}, function(data){
		jQuery('#option-group').append('<option value="500"> - </option>');
		jQuery.each(data,function(i,value){
			inAttr	= "";
			if(grupId==value.grup_id){
				inAttr	= "selected";
			}
			jQuery('#option-group').append('<option value="' + value.grup_id + '" ' + inAttr + '>' + value.grup_nama + '</option>');
		});
	}, "json");
}

// fungsi form select dpd
function getSelectDpd(dpdKode){
	targetUrl  	= "/api/perpamsi/referensi/view_dpd.php";
	jQuery.post(targetUrl, {}, function(data) {
		jQuery('#option-dpd').append('<option value="100"> - </option>');
		jQuery.each(data,function(i,value){
			inAttr	= "";
			if(dpdKode==value.dpd_kode){
				inAttr	= "selected";
			}
			jQuery('#option-dpd').append('<option value="' + value.dpd_kode + '" ' + inAttr + '>' + value.dpd_nama + '</option>');
		});
	}, "json");
}

// fungsi form select pdam
function getSelectPdam(dpdKode,pdamKode){
	inHTML		= "";
	targetUrl	= "/api/perpamsi/referensi/view_pdam.php";
	jQuery.post(targetUrl, {filter: [{name: "dpd_kode", value: dpdKode}]}, function(data){
		inHTML 	= inHTML + '<option value="' + dpdKode + '0000"> - </option>';
		jQuery.each(data,function(i,value){
			inAttr		= "";
			if(pdamKode==value.pdam_kode){
				inAttr	= "selected";
			}
			inHTML = inHTML + '<option value="' + value.pdam_kode + '" ' + inAttr + '>'+ value.pdam_nama +'</option>';
		});
		jQuery('#option-bpam').html(inHTML);
	}, "json");
};

// funcsi form kemhan
function getUnitOrganisasi(gpKode){
	targetUrl   = "/api/perpamsi/referensi/view_kemhan.php";
	jQuery.post(targetUrl, {}, function(data) {
		jQuery('#option-kemhan').append('<option value="000000"> - </option>');
		jQuery.each(data,function(i,value){
			inAttr	= "";
			if(gpKode==value.gp_kode){
				inAttr	= "selected";
			}
			jQuery('#option-kemhan').append('<option value="'+ value.gp_kode +'" ' + inAttr + '>'+ value.gp_nama +'</option>');
		});
	}, "json");
}

// fungsi form kotama
function getKotama(gpKode, gpParent){
	targetUrl  	= '/api/perpamsi/referensi/view_kotama.php';
	inHTML		= '<option value="' + gpKode + '" > - </option>';
	jQuery.post(targetUrl, {filter: [{name: "gp_parent", value: gpParent}]}, function(data){
		jQuery.each(data,function(i,value){
			inAttr	= "";
			if(gpKode==value.gp_kode){
				inAttr	= "selected";
			}
			inHTML 	= inHTML + '<option value="'+ value.gp_kode +'" ' + inAttr + '>'+ value.gp_nama +'</option>';
		});
		jQuery('#option-kotama').html(inHTML) ;
	}, "json");
};

// load menu utama
var applName	= "";
var targetDir	= "";
function loadMenu(param){
	dataTemp	= JSON.parse(localStorage.perpamsi);
	dataFeed	= dataTemp[param];
	applName	= dataFeed.applName;
	targetDir	= dataFeed.targetUrl
	inHTML		= '<i class="fa fa-html5"></i>' + dataFeed.applName + '<br><small>' + dataFeed.applDesc + '</small>';

	// breadcrumb
	if(!jQuery('.breadcrumb').hasClass('hide')){
		jQuery('.breadcrumb').addClass('hide');
	}

	// set menu aktif
	targetUrl			= dataFeed.targetUrl + 'index.html';
	dataTemp.applKode	= param;
	delete dataTemp.procKode;
	delete dataTemp.proses;
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	jQuery('#page-content div div h1').html(inHTML);
	jQuery('.block').load(targetUrl);
}

// nama bulan
function namaBulan(param){
	switch(parseInt(param)){
		case 1:
			return 'JANUARI';
			break;
		case 2:
			return 'FEBRUARI';
			break;
		case 3:
			return 'MARET';
			break;
		case 4:
			return 'APRIL';
			break;
		case 5:
			return 'MEI';
			break;
		case 6:
			return 'JUNI';
			break;
		case 7:
			return 'JULI';
			break;
		case 8:
			return 'AGUSTUS';
			break;
		case 9:
			return 'SEPTEMBER';
			break;
		case 10:
			return 'OKTOBER';
			break;
		case 11:
			return 'NOPEMBER';
			break;
		case 12:
			return 'DESEMBER';
			break;
		default:
			return 'PILIH BULAN';
	}
}

// load proses in-menu
function loadFile(param){
	dataTemp	= JSON.parse(localStorage.perpamsi);
	dataFeed	= {proses: dataTemp.proses[param].proses};
	targetUrl	= dataTemp[dataTemp.applKode].targetUrl + 'index.html';

	// set proses aktif
	dataTemp.procKode	= param;
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));

	jQuery('.block').load(targetUrl);
}

// load modal form
var dataTemp	= {};
var dataFeed	= {};
var targetUrl   = "";
function loadForm(param){
	dataTemp			= JSON.parse(localStorage.perpamsi);
	dataFeed.targetId	= getToken();
	jQuery('input.' + param).each(function(i,value){
		dataFeed[value.name] = value.value;
	});

	if(dataFeed.proses=='gantiPassword'){
		targetUrl	= dataFeed.targetUrl;
	}
	else if(typeof(dataTemp.applKode)=='string'){
		targetUrl	= dataTemp[dataTemp.applKode].targetUrl + dataFeed.targetUrl;
	}
	else{
		targetUrl	= dataFeed.targetUrl;
	}
	jQuery('body').prepend('<div id="' + dataFeed.targetId + '" class="modal fade"></div>');
	jQuery('#' + dataFeed.targetId).load(targetUrl);
}

function loadProc(param){
	targetUrl	= param.targetUrl;
	delete param.targetUrl;
	jQuery.post(targetUrl, param, function(data){
		jQuery('form').html(data);
	});
}

// generate menu aplikasi
var kunci		= "";
var randId		= "";
var inHTML		= "";
var inAttr		= "";
var dataProc	= {};

targetUrl   	= "/api/perpamsi/get_menu.php";
jQuery.post(targetUrl, dataFeed, function(data){
	jQuery.each(data, function(key, value){
		kunci = key;
		jQuery.each(value, function(key, value){
			if(kunci=='000000'){
				if(value.appl_kode.substr(4,2)=='00'){
					inHTML = '<li onclick="toggleMenu(\'' + value.appl_kode + '\')"><a class="sidebar-nav-menu open" style="cursor: pointer"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="fa fa-th-large sidebar-nav-icon"></i>' + value.appl_name + '</a><ul class="' + value.appl_kode + '" style="display: none"></ul></li>';
				}
			}
			else{
				dataTemp[value.appl_kode]	= {targetUrl: value.appl_file, applKode: value.appl_kode, applName: value.appl_name, applDesc: value.appl_deskripsi};
				inHTML = '<li><a onclick="loadMenu(\'' + value.appl_kode + '\')" style="cursor: pointer">' + value.appl_name + '</a></li>';
			}
			jQuery('.' + kunci).append(inHTML);
		});
	});
	localStorage.setItem("perpamsi", JSON.stringify(dataTemp));
}, "json");
