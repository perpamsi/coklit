/*
 *  Document   : app.js
 *  Author     : pixelcave
 */
var Sid=function()
{
	var e=$("#page-container"),
		t=$("header"),
		a=$("#sidebar-toggle-sm"),
		i=$("#il"),
		o=function(){l("onot");
		},



		l=function(o)

		{ "onot"===o?((t.hasClass("navbar-fixed-top")||t.hasClass("navbar-fixed-bottom"))&&l("init-scroll"),

		a.click(function(){l("toggle-sm")}),
		i.click(function(){l("toggle-lg")}))
		:"toggle-lg"===o?e.toggleClass("sidebar-full")
		:"toggle-sm"===o?e.toggleClass("sidebar-open")
		:"open-sm"===o?e.addClass("sidebar-open")
		:"close-sm"===o?e.removeClass("sidebar-open")
		:"open-lg"===o?e.addClass("sidebar-full")
		:"close-lg"===o?e.removeClass("sidebar-full")
		:"init-scroll"==o?s.length&&!s.parent(".slimScrollDiv").length&&(s.slimScroll({height:$(window).height(),
		color:"#fff",
		size:"3px",
		touchScrollStep:100}),
		$(window).resize(r),
		$(window).bind("orientationchange",d))
		:"destroy-scroll"==o&&s.parent(".slimScrollDiv").length&&(s.parent().replaceWith(function(){return $(this).contents()}),s=$(".sidebar-scroll"),s.removeAttr("style"),$(window).off("resize",r),
		$(window).unbind("orientationchange",d)) 

		};


		return{

			onot:function(){o()},
		}

		} ();

$(function(){Sid.onot()});