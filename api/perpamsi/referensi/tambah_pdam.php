<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['data'];
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name']	= $nilai[$i]['value'];
	}
	/* getParam **/

	$error	= "";
	$que	= "";
	if(strlen($pdam_kode)==7){
		try{
			$PLINK->beginTransaction();
			$que	= "INSERT INTO tm_pdam(pdam_kode,dpd_kode,pdam_nama,pdam_kota,pdam_alamat,pdam_bank,pdam_an,pdam_no_rek) VALUES('".$pdam_kode."',SUBSTR('".$pdam_kode."',1,3),'".$pdam_nama."','".$pdam_kota."','".$pdam_alamat."','".$pdam_bank."','".$pdam_an."','".$pdam_no_rek."')";
			if($PLINK->exec($que)>0){
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal disimpan";
			$kelas	= "alert alert-warning";
			$error	= $e->getMessage();
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que);
	echo json_encode($pesan);
?>
