<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Credentials: true');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$error	= "";
	$query	= "DELETE FROM tm_pelanggan ";
	$nilai	= $_POST['filter'];
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name']	= $nilai[$i]['value'];
	}
	$query	.= " WHERE pel_no='".$pel_no."'";
	/* getParam **/

	session_start();
	if(count($_SESSION)>0){
		try{
			$PLINK->beginTransaction();
			if($PLINK->exec($query)>0){
				$pesan 	= "Data telah berhasil dihapus";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa dihapus";
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal dihapus";
			$kelas	= "alert alert-warning";
			$error	= $e->getMessage();
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diproses";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query);
	echo json_encode($pesan);
?>
