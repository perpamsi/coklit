<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: http://'.$_SERVER['REMOTE_ADDR']);
	header('Access-Control-Allow-Credentials: true');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";
	session_start();
	define('_GRUP', $_SESSION['Grup_c']);

	/* database **/
	try {
		$que 	= "SELECT parent_id AS ga_kode,appl_proc,appl_kode,appl_file,appl_name,appl_deskripsi FROM v_menu_item WHERE getmenu('"._GRUP."',appl_kode)>0";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data menu gagal dilakukan", "error"=>$e->getMessage(), "errno"=>1, "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
