<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	session_start();
	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_SESSION['Kota_c'])){
		$pdam_kode = $_SESSION['Kota_c'];
		if(substr($pdam_kode,0,3)=='100'){
			$filter	= "";
		}
		else if(substr($pdam_kode,3,4)=='0000'){
			$_POST['filter'][]	= array("name"=>"dpd_kode", "value"=>substr($pdam_kode,0,3));
		}
		else{
			$_POST['filter'][]	= array("name"=>"pdam_kode", "value"=>$pdam_kode);
		}

		if(isset($_POST['filter'])){
			$kolom	= array();
			$nilai	= $_POST['filter'];
			for($i=0;$i<count($nilai);$i++){
				$kolom[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			}
			if($i>0){
				$filter	= "WHERE ".implode(' AND ',$kolom);
			}
		}
	}
	else{
		$filter	= "LIMIT 0";
	}
	/* getParam **/

	/* database **/
	try {
		$que 	= "SELECT pel_no,pel_nama,pdam_kode,gp_nama,bln_tagihan,bln_rekening,SUM(rek_lembar) AS rek_lembar,SUM(rek_st_pakai) AS rek_st_pakai,SUM(rek_total) AS rek_total FROM view_drd_tunda ".$filter." GROUP BY pel_no";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data pdam gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
