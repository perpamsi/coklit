<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	session_start();
	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";
	$error		= "";
	$query		= "";
	$rek_sts	= 1;
	$pdam_kode	= "";
	$pel_no		= "";
	if(isset($_SESSION['Kota_c'])){
		$usr_id		= $_SESSION['User_c'];
		$pdam_kode 	= $_SESSION['Kota_c'];
		if(substr($pdam_kode,0,3)=='100'){
			$rek_sts	= 4;
		}
		else if(substr($pdam_kode,3,4)=='0000'){
			$rek_sts	= 3;
		}
		else{
			$rek_sts	= 2;
		}
		/** getParam
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$$nilai[$i]['name']	= $nilai[$i]['value'];
		}
		/* getParam **/
	}

	function getToken(){
		$acak	= mt_rand(1,9999);
		return date('ymdHis').str_repeat('0',4-strlen($acak)).$acak;
	}
	$token	= getToken();

	if($rek_sts>1){
		try{
			$PLINK->beginTransaction();
/*
			update rekening bulan berjalan untuk setiap rekening yang belum disetujui
			1 	= Input rekening
			2 	= Validasi PDAM
			3 	= Validasi DPD
			4 	= Validasi DPP
			5 	= Sudah Disetujui
			6 	= Tunda Administratif
			7 	= Tunda Penarikan Berkas
			8	= Klarifikasi
			10 	= Tolak
*/
			$query	= "UPDATE tm_rekening SET rek_sts=".$rek_sts." WHERE pel_no='".$pel_no."' AND pdam_kode='".$pdam_kode."' AND rek_sts<".$rek_sts." AND MONTH(rek_tgl)=MONTH(NOW()) AND YEAR(rek_tgl)=YEAR(NOW())";

			if($PLINK->exec($query)>0){
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "alert alert-success";
			}
			else{
				if(!isset($pesan)){
					$pesan 	= "Approval data tidak bisa disimpan";
				}
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal disimpan";
			$kelas	= "alert alert-warning";
			$error	= $e->getMessage();
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query);
	echo json_encode($pesan);
?>
