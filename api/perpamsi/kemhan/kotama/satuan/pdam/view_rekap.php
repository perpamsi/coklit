<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB01.php";
	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['filter'];
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name']	= $nilai[$i]['value'];
	}
	/* getParam **/
	if($gp_kode=='010000'){
		$filter = "WHERE MONTH(a.rek_tgl)=".$bulan." AND YEAR(a.rek_tgl)=".$tahun;
	}
	else{
		$filter = "WHERE b.gp_parent='".$gp_kode."' AND MONTH(a.rek_tgl)=".$bulan." AND YEAR(a.rek_tgl)=".$tahun;
	}
	
	/* database **/
	try {
		$que 	= "SELECT c.gp_kode,d.pdam_kode,b.pel_no,c.gp_nama,d.pdam_nama,b.pel_nama,SUM(a.rek_lembar) AS rek_lembar,SUM(isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar)) AS rek_lembar00,SUM(isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar)) AS rek_lembar01,SUM(isM05(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar,a.rek_sts)) AS rek_lembar05,SUM(IF(a.rek_sts=6,a.rek_lembar,0)) AS klarifikasi_lembar,SUM(a.rek_st_pakai) AS rek_st_pakai,SUM(isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai)) AS rek_st_pakai00,SUM(isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai)) AS rek_st_pakai01,SUM(isM05(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai,a.rek_sts)) AS rek_st_pakai05,SUM(IF(a.rek_sts=6,a.rek_st_pakai,0)) AS klarifikasi_st_pakai,SUM(a.rek_total) AS rek_total,SUM(isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total)) AS rek_total00,SUM(isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total)) AS rek_total01,SUM(isM05(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total,a.rek_sts)) AS rek_total05,SUM(IF(a.rek_sts=6,a.rek_total,0)) AS klarifikasi_total FROM tm_rekening a JOIN tm_pelanggan b ON(b.pel_no=a.pel_no) JOIN tm_grup_pelanggan c ON(c.gp_kode=b.gp_kode) JOIN tm_pdam d ON(d.pdam_kode=b.pdam_kode) ".$filter." GROUP BY c.gp_kode,d.pdam_kode,b.pel_no";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
