<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	session_start();
	$usr_id		= $_SESSION['User_c'];
	$error		= "";
	$query		= "";

	function getToken(){
		$acak	= mt_rand(1,9999);
		return date('ymdHis').str_repeat('0',4-strlen($acak)).$acak;
	}
	$token	= getToken();

	try{
		$PLINK->beginTransaction();
/*
	update rekening bulan berjalan untuk setiap rekening yang belum disetujui
	0	= Void rekening
	1 	= Input rekening
	2 	= Validasi PDAM
	3 	= Validasi DPD
	4 	= Validasi DPP
	5 	= Sudah Disetujui
	6	= Sudah Klarifikasi
	7 	= Tunda Administratif
	8 	= Tunda Penarikan Berkas
	9 	= Tolak
*/
		// tunda rekening : update status : 9
		$query	= "UPDATE tm_rekening SET usr_id='".$usr_id."',rek_sts=IF(pel_no=pel_nosl,9,0),remark_id=:remark_id WHERE rek_nomor=:rek_nomor AND rek_sts<5 AND rek_sts>0";
		$stmt	= $PLINK->prepare($query);

		/** getParam 
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		$j		= 0;
		$data	= $_POST['filter'];
		for($k=0;$k<count($data);$k++){
			$nilai			= $data[$k];
			$$nilai['name']	= $nilai['value'];

			$stmt -> bindParam(':remark_id', $token);
			$stmt -> bindParam(':rek_nomor', $rek_nomor);
			$stmt -> execute();
			if($stmt -> rowCount()>0){
				$j++;
			}
			$token++;
		}
		/* getParam **/

		if($j>0){
			$pesan 	= "Data telah berhasil disimpan";
			$kelas	= "alert alert-success";
		}
		else{
			$pesan 	= "Data tidak bisa disimpan";
			$kelas	= "alert alert-info";
		}
		$PLINK->commit();
	}
	catch(Exception $e){
		$PLINK->rollBack();
		$pesan	= "Data gagal disimpan";
		$kelas	= "alert alert-warning";
		$error	= $e->getMessage();
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query);
	echo json_encode($pesan);
?>
