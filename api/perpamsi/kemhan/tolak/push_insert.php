<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	session_start();
	$usr_id	= $_SESSION['User_c'];

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	function getToken(){
		$acak	= mt_rand(1,9999);
		return date('ymdHis').str_repeat('0',4-strlen($acak)).$acak;
	}
	$token	= getToken();
	$error	= "";
	$query	= "";
	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['filter'];
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name']	= $nilai[$i]['value'];
	}
	/* getParam **/

	if(count($_POST['data'])>0){
		try{
			$PLINK->beginTransaction();
			// delete rekening tunda bulan berjalan
			$query	= "DELETE FROM tm_rekening WHERE pel_no='".$pel_no."' AND pdam_kode='".$pdam_kode."' AND YEAR(rek_tgl)=YEAR(NOW()) AND MONTH(rek_tgl)=MONTH(NOW()) AND rek_sts=9";
			$PLINK->exec($query);

			// tambah rekening bulan berjalan
			$query	= "INSERT INTO tm_rekening(rek_nomor,pel_nosl,pel_no,pdam_kode,rek_bln,rek_thn,rek_tgl,usr_id,rek_st_pakai,rek_total,rek_lembar,rek_sts,remark_id) VALUES(:rek_nomor,:pel_nosl,:pel_no,:pdam_kode,:rek_bln,:rek_thn,NOW(),:usr_id,:rek_st_pakai,:rek_total,1,9,:remark_id)";
			$stmt	= $PLINK->prepare($query);
			$j		= 0;
			$data	= $_POST['data'];
			for($k=0;$k<count($data);$k++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$k];
				$konci	= array_keys($nilai);
				for($i=0;$i<count($konci);$i++){
					$$konci[$i]	= $nilai[$konci[$i]];
				}
				/* getParam **/
				if(abs($rek_bln)>=1 && abs($rek_bln)<=12 && $rek_thn<=date('Y') && $rek_thn>2000){
					$stmt -> bindParam(':remark_id', $token);
					$stmt -> bindParam(':rek_nomor', $token);
					$stmt -> bindParam(':pel_nosl', $pel_nosl);
					$stmt -> bindParam(':pel_no', $pel_no);
					$stmt -> bindParam(':pdam_kode', $pdam_kode);
					$stmt -> bindParam(':rek_bln', $rek_bln);
					$stmt -> bindParam(':rek_thn', $rek_thn);
					$stmt -> bindParam(':usr_id', $usr_id, PDO::PARAM_STR);
					$stmt -> bindParam(':rek_st_pakai', $rek_st_pakai);
					$stmt -> bindParam(':rek_total', $rek_total);
					$stmt -> execute();
					if($stmt -> rowCount()>0){
						$j++;
					}
				}
				$token++;
			}

			if($j>0){
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$pesan	= "Data gagal disimpan";
			$kelas	= "alert alert-warning";
			$error	= $e->getMessage();
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query);
	echo json_encode($pesan);
?>
