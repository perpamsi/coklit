<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	/* database **/
	try {
		$que 	= "SELECT dpd_kode,dpd_nama,SUM(rek_lembar) AS rek_lembar,SUM(rek_st_pakai) AS rek_st_pakai,SUM(rek_total) AS rek_total FROM view_drd_valid GROUP BY dpd_kode";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data pdam gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
