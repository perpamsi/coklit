<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$filter	= array();
	$nilai	= $_POST['filter'];
	for($i=0;$i<count($nilai);$i++){
		$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
	}
	if($i>0){
		$filter	= "WHERE ".implode(' AND ',$filter);
	}
	/* getParam **/

	/* database **/
	try {
		$que 	= "SELECT *,rek_bln AS bulan,rek_thn AS tahun,rek_lembar AS lembar,rek_st_pakai AS volume,rek_total AS rupiah FROM view_drd_valid ".$filter;
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data pdam gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
