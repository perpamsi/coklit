<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB01.php";
	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
		$que 	= "SELECT tagihan,pdam_kode,rek_bln,rek_thn,bln_tagihan,SUM(rek_lembar) AS rek_lembar,SUM(rek_st_pakai) AS rek_st_pakai,SUM(rek_total) AS rek_total FROM view_drd_arsip ".$filter." GROUP BY YEAR(rek_tgl),MONTH(rek_tgl) ORDER BY rek_thn,rek_bln";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
