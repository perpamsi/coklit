<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB01.php";
	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	$filter	= "WHERE MONTH(a.rek_tgl)=4 AND YEAR(a.rek_tgl)=2015";
	/* getParam **/
	
	/* database **/
	try {
		$que 	= "SELECT c.gp_kode,c.gp_nama,SUM(a.rek_lembar) AS rek_lembar,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),a.rek_lembar,0)) AS tunda_lembar,SUM(IF(a.rek_sts=9,a.rek_lembar,0)) AS tolak_lembar,SUM(IF(a.rek_sts=6,a.rek_lembar,0)) AS klarifikasi_lembar,SUM(a.rek_st_pakai) AS rek_st_pakai,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),a.rek_st_pakai,0)) AS tunda_st_pakai,SUM(IF(a.rek_sts=9,a.rek_st_pakai,0)) AS tolak_st_pakai,SUM(IF(a.rek_sts=6,a.rek_st_pakai,0)) AS klarifikasi_st_pakai,SUM(a.rek_total) AS rek_total,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),a.rek_total,0)) AS tunda_total,SUM(IF(a.rek_sts=9,a.rek_total,0)) AS tolak_total,SUM(IF(a.rek_sts=6,a.rek_total,0)) AS klarifikasi_total FROM tm_rekening a JOIN tm_pelanggan b ON(b.pel_no=a.pel_no) JOIN tm_grup_pelanggan c ON(c.gp_kode=b.gp_parent) ".$filter." GROUP BY c.gp_kode";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
