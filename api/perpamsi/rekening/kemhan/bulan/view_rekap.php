<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB01.php";
	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	$filter	= "WHERE MONTH(a.rek_tgl)=4 AND YEAR(a.rek_tgl)=2015";
	/* getParam **/
	
	/* database **/
	try {
		$que 	= "SELECT c.gp_kode,c.gp_nama,SUM(a.rek_lembar) AS rek_lembar,SUM(isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar)) AS rek_lembar00,SUM(isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar)) AS rek_lembar01,SUM(isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar)) AS rek_lembar02,SUM(isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar)) AS rek_lembar04,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),a.rek_lembar,0)) AS tunda_lembar,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tunda_lembar00,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tunda_lembar01,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tunda_lembar02,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tunda_lembar04,SUM(IF(a.rek_sts=9,a.rek_lembar,0)) AS tolak_lembar,SUM(IF(a.rek_sts=9,isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tolak_lembar00,SUM(IF(a.rek_sts=9,isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tolak_lembar01,SUM(IF(a.rek_sts=9,isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tolak_lembar02,SUM(IF(a.rek_sts=9,isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_lembar),0)) AS tolak_lembar04,SUM(IF(a.rek_sts=6,a.rek_lembar,0)) AS klarifikasi_lembar,SUM(a.rek_st_pakai) AS rek_st_pakai,SUM(isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai)) AS rek_st_pakai00,SUM(isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai)) AS rek_st_pakai01,SUM(isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai)) AS rek_st_pakai02,SUM(isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai)) AS rek_st_pakai04,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),a.rek_st_pakai,0)) AS tunda_st_pakai,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tunda_st_pakai00,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tunda_st_pakai01,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tunda_st_pakai02,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tunda_st_pakai04,SUM(IF(a.rek_sts=9,a.rek_st_pakai,0)) AS tolak_st_pakai,SUM(IF(a.rek_sts=9,isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tolak_st_pakai00,SUM(IF(a.rek_sts=9,isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tolak_st_pakai01,SUM(IF(a.rek_sts=9,isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tolak_st_pakai02,SUM(IF(a.rek_sts=9,isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_st_pakai),0)) AS tolak_st_pakai04,SUM(IF(a.rek_sts=6,a.rek_st_pakai,0)) AS klarifikasi_st_pakai,SUM(a.rek_total) AS rek_total,SUM(isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total)) AS rek_total00,SUM(isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total)) AS rek_total01,SUM(isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total)) AS rek_total02,SUM(isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total)) AS rek_total04,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),a.rek_total,0)) AS tunda_total,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tunda_total00,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tunda_total01,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tunda_total02,SUM(IF((a.rek_sts=7 OR a.rek_sts=8),isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tunda_total04,SUM(IF(a.rek_sts=9,a.rek_total,0)) AS tolak_total,SUM(IF(a.rek_sts=9,isM00(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tolak_total00,SUM(IF(a.rek_sts=9,isM01(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tolak_total01,SUM(IF(a.rek_sts=9,isM02(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tolak_total02,SUM(IF(a.rek_sts=9,isM04(CURDATE(),a.rek_bln,a.rek_thn,a.rek_total),0)) AS tolak_total04,SUM(IF(a.rek_sts=6,a.rek_total,0)) AS klarifikasi_total FROM tm_rekening a JOIN tm_pelanggan b ON(b.pel_no=a.pel_no) JOIN tm_grup_pelanggan c ON(c.gp_kode=b.gp_parent) ".$filter." GROUP BY c.gp_kode";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
