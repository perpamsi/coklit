<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_POST['filter'])){
		$filter	= array();
		$nilai	= $_POST['filter'];
		for($i=0;$i<count($nilai);$i++){
			$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
		}
		if($i>0){
			$filter	= "WHERE ".implode(' AND ',$filter);
		}
	}
	else{
		$filter	= "";
	}
	/* getParam **/

	/* database **/
	try {
		$que 	= "SELECT gp_parent,kotama,gp_kode,ABS(SUBSTR(gp_kode,-2)) AS gp_roman,gp_nama,SUM(rek_lembar) AS rek_lembar,SUM(rek_st_pakai) AS rek_st_pakai,SUM(rek_total) AS rek_total FROM view_drd_valid ".$filter." GROUP BY gp_kode";
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data pdam gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
