<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	session_start();
	$usr_id			= $_SESSION['User_c'];
	$pdam_kode 	= $_SESSION['Kota_c'];
	if(substr($pdam_kode,0,3)=='100'){
		$rek_sts	= 4;
	}
	else if(substr($pdam_kode,3,4)=='0000'){
		$rek_sts	= 3;
	}
	else{
		$rek_sts	= 1;
	}

	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB02.php";

	function getToken(){
		$acak	= mt_rand(1,9999);
		return date('ymdHis').str_repeat('0',4-strlen($acak)).$acak;
	}
	$token	= getToken();
	$error	= "";
	$errno	= "";
	$query	= "";
	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['filter'];
	for($i=0;$i<count($nilai);$i++){
		$$nilai[$i]['name']	= $nilai[$i]['value'];
	}
	/* getParam **/

	if(count($_POST['data'])>0){
		try{
			$PLINK->beginTransaction();
			// delete rekening bulan berjalan
			$query	= "DELETE FROM tm_rekening WHERE pel_no='".$pel_no."' AND pdam_kode='".$pdam_kode."' AND rek_sts>0 AND rek_sts<=".$rek_sts;
			$PLINK->exec($query);

			// tambah rekening bulan berjalan
			$query	= "INSERT INTO tm_rekening(rek_nomor,pel_nosl,pel_no,pdam_kode,rek_bln,rek_thn,rek_tgl,usr_id,rek_st_pakai,rek_total,rek_lembar,rek_sts,remark_id) VALUES(:rek_nomor,:pel_nosl,:pel_no,:pdam_kode,:rek_bln,:rek_thn,NOW(),:usr_id,:rek_st_pakai,:rek_total,:rek_lembar,:rek_sts,:remark_id)";
			$stmt	= $PLINK->prepare($query);
			$j		= 0;
			$data	= $_POST['data'];
			for($k=0;$k<count($data);$k++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$k];
				$konci	= array_keys($nilai);
				for($i=0;$i<count($konci);$i++){
					$$konci[$i]	= $nilai[$konci[$i]];
				}
				/* getParam **/
				if(abs($bulan)>=1 && abs($bulan)<=12 && $tahun<=date('Y') && $tahun>2000){
					$stmt -> bindParam(':remark_id', $token);
					$stmt -> bindParam(':rek_nomor', $token);
					$stmt -> bindParam(':pel_nosl', $pel_no);
					$stmt -> bindParam(':pel_no', $pel_no);
					$stmt -> bindParam(':pdam_kode', $pdam_kode);
					$stmt -> bindParam(':rek_bln', $bulan);
					$stmt -> bindParam(':rek_thn', $tahun);
					$stmt -> bindParam(':usr_id', $usr_id, PDO::PARAM_STR);
					$stmt -> bindParam(':rek_st_pakai', $volume);
					$stmt -> bindParam(':rek_total', $rupiah);
					$stmt -> bindParam(':rek_lembar', $lembar);
					$stmt -> bindParam(':rek_sts', $rek_sts);
					$stmt -> execute();
					if($stmt -> rowCount()>0){
						$j++;
					}
				}
				$token++;
			}

			if($j>0){
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "alert alert-success";
			}
			else{
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "alert alert-info";
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$kelas	= "alert alert-warning";
			$error	= $e->getMessage();
			$errno	= $e->getCode();
			$pesan	= "Data gagal disimpan";
			if($errno == 23000){
				$pesan	= "Data gagal disimpan karena terjadi duplikasi data rekening";
			}
		}
	}
	else{
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "alert alert-warning";
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "errno"=>$errno, "query"=>$query);
	echo json_encode($pesan);
?>
