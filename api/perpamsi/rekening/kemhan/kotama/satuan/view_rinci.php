<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	session_start();
	include $_SERVER['DOCUMENT_ROOT']."/api/perpamsi/setDB01.php";

	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	if(isset($_SESSION['Kota_c'])){
		$pdam_kode 	= $_SESSION['Kota_c'];
		$rek_sts		= 1;
		if(substr($pdam_kode,0,3)=='100'){
			$filter		= "";
			$rek_sts	= 4;
		}
		else if(substr($pdam_kode,3,4)=='0000'){
			$_POST['filter'][]	= array("name"=>"dpd_kode", "value"=>substr($pdam_kode,0,3));
			$rek_sts	= 3;
		}
		else{
			$_POST['filter'][]	= array("name"=>"pdam_kode", "value"=>$pdam_kode);
		}

		if(isset($_POST['filter'])){
			$kolom	= array();
			$nilai	= $_POST['filter'];
			for($i=0;$i<count($nilai);$i++){
				$kolom[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			}
			if($i>0){
				$filter	= "WHERE ".implode(' AND ',$kolom);
			}
		}
	}
	else{
		$filter	= "LIMIT 0";
	}
	/* getParam **/

	/* database **/
	try {
		$que 	= "SELECT *,rek_bln AS bulan,rek_thn AS tahun,rek_lembar AS lembar,rek_st_pakai AS volume,rek_total AS rupiah,IF(IFNULL(rek_sts,0)<".$rek_sts.",1,0) AS approved FROM view_drd_input ".$filter;
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
        $row    = array("pesan"=>"Inquiry data pdam gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
